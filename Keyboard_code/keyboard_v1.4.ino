// mutantC keyboard v1.5

#include <Keypad.h>
#include <Keyboard.h>
#include <Mouse.h>

const byte ROWS = 4;
const byte COLS = 11;
int state = 0;

char hexaAlphabets[ROWS][COLS] = {
  {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ' '},
  {'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', KEY_BACKSPACE},
  {KEY_CAPS_LOCK, 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',  KEY_RETURN},
  {KEY_LEFT_CTRL, KEY_LEFT_GUI,  'z', 'x', 'c', 'v', 'b', 'n', 'm', ' ', 'E'  }
};

char hexaSymbols[ROWS][COLS] = {
  {KEY_ESC, '!', '@', '#', '$', '%', '^', '&', '(', ')', ' '},
  {KEY_TAB, '|', KEY_UP_ARROW , '_', '~', ':', ';', '\"', '\'', '`', KEY_BACKSPACE},
  {KEY_CAPS_LOCK, KEY_LEFT_ARROW, KEY_DOWN_ARROW, KEY_RIGHT_ARROW,  '[', ']','{', '}', '<', '>',  KEY_RETURN},
  {KEY_LEFT_CTRL, KEY_LEFT_GUI, '+', '-', '/', '*', '=', ',', '.', '?', 'E'}
};

char hexaGamePad[ROWS][COLS] = {
  {KEY_ESC, ' ', KEY_UP_ARROW, ' ', ' ', 'r', ' ', 'X', ' ', 'L', ' '},
  {' ', KEY_LEFT_ARROW, ' ', KEY_RIGHT_ARROW, ' ', ' ', 'Y',' ', 'A', 'R', KEY_BACKSPACE},
  {' ', ' ', KEY_DOWN_ARROW, ' ', ' ', ' ', ' ','B' , ' ' , ' ', KEY_RETURN},
  {KEY_LEFT_CTRL, KEY_LEFT_GUI, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'E'  }
};

byte rowPins[ROWS] = {A0, A1, A3, A2};
byte colPins[COLS] = {9, 16, 14, 15, 8, 7, 6, 5, 4, 3, 2};

Keypad Alphabets  = Keypad(makeKeymap(hexaAlphabets), rowPins, colPins, ROWS, COLS);
Keypad Symbols    = Keypad(makeKeymap(hexaSymbols), rowPins, colPins, ROWS, COLS);
Keypad GamePad    = Keypad(makeKeymap(hexaGamePad), rowPins, colPins, ROWS, COLS);

void setup(){
  Serial.begin(9600);
}

void loop() {
  char key;
  if (state == 0) {
    key = Alphabets.getKey();
  } else if (state == 1) {
    key = Symbols.getKey();
  } else if (state == 2) {
    key = GamePad.getKey();
  }

  // Activation key for symbol
  char symbolAct = 'E';
  char leftAct = 'L';
  char rightAct = 'R';
  
  Keyboard.begin();
  if (key) {
    if (key != symbolAct && key != leftAct && key != rightAct  ) {
      Keyboard.write(key);
      delay(200);
    }
    
    if (key == symbolAct) { 
      // Change the state
      if (state == 0) {         // latter layout
        state = 1;
      } else if (state == 1) {  // symbol layout
        state = 2;
      } else if (state == 2) {  // gamepad
        state = 0;
      }
    } else if( key == leftAct){  // left mouse click
      Mouse.press(MOUSE_LEFT);
      delay(300);
      Mouse.release(MOUSE_LEFT);
      delay(200);
    } else if( key == rightAct){ // right mouse click
      Mouse.press(MOUSE_RIGHT);
      delay(300);
      Mouse.release(MOUSE_RIGHT);
      delay(200);
    }
  }
  
  Keyboard.end();
}