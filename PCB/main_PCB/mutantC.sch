<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="mPads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="mUnrouted" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="mDimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="mbStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-Switches" urn="urn:adsk.eagle:library:12047409">
<description>&lt;h3&gt;SparkFun Switches, Buttons, Encoders&lt;/h3&gt;
In this library you'll find switches, buttons, joysticks, and anything that moves to create or disrupt an electrical connection.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TACTILE_SWITCH_PTH_6.0MM" urn="urn:adsk.eagle:footprint:12047461/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 6.0mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-1000)&lt;/p&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="0" y="3.302" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_4.5MM" urn="urn:adsk.eagle:footprint:12047462/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 4.5mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://spec_sheets.e-switch.com/specs/P010338.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.3" y1="2.2" x2="-1.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="-2.2" x2="1.3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="0.8" x2="1.8" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="1.8" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="-2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="-2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<text x="0" y="2.413" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_12MM" urn="urn:adsk.eagle:footprint:12047463/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 12mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-5050)&lt;/p&gt;</description>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
<text x="0" y="6.223" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-6.223" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_6.0X3.5MM" urn="urn:adsk.eagle:footprint:12047464/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 6.0 x 3.5 mm&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/1101.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-3" y1="1.1" x2="-3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="3" y1="1.1" x2="3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.75" y1="1.75" x2="-3" y2="1.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.75" y1="1.75" x2="2.75" y2="1.75" width="0.2032" layer="21"/>
<wire x1="2.75" y1="1.75" x2="3" y2="1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="3" y1="-1.5" x2="2.75" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.75" y1="-1.75" x2="-2.75" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.5" x2="-2.75" y2="-1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-3" y1="-1.5" x2="-3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="1.1" x2="3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="-1.5" x2="3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2" y1="0" x2="-1" y2="0" width="0.127" layer="51"/>
<wire x1="-1" y1="0" x2="0.1" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.3" y1="0" x2="2" y2="0" width="0.127" layer="51"/>
<smd name="1" x="-3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<smd name="2" x="3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.905" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_6.2MM_TALL" urn="urn:adsk.eagle:footprint:12047466/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 6.2mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.apem.com/files/apem/brochures/ADTS6-ADTSM-KTSC6.pdf"&gt;Datasheet&lt;/a&gt; (ADTSM63NVTR)&lt;/p&gt;</description>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.2032" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.2032" layer="21"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.2032" layer="21"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.2032" layer="21"/>
<smd name="A1" x="-3.975" y="-2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="A2" x="3.975" y="-2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="B1" x="-3.975" y="2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="B2" x="3.975" y="2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<text x="0" y="3.175" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_RIGHT_ANGLE_KIT" urn="urn:adsk.eagle:footprint:12047468/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://cdn.sparkfun.com/datasheets/Components/Switches/SW016.JPG"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="1.5" y1="-3.8" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="3.5" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="3.5" x2="3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.65" y1="3.5" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="3" y2="2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="2" x2="3" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.777" y1="1" x2="-3.777" y2="-2.127" width="0.2032" layer="21"/>
<wire x1="-3.777" y1="-2.127" x2="3.777" y2="-2.127" width="0.2032" layer="21"/>
<wire x1="3.777" y1="-2.127" x2="3.777" y2="1" width="0.2032" layer="21"/>
<wire x1="2" y1="2.127" x2="-2" y2="2.127" width="0.2032" layer="21"/>
<pad name="ANCHOR1" x="-3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="ANCHOR2" x="3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<circle x="2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<circle x="2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="-2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_12MM" urn="urn:adsk.eagle:footprint:12047469/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 12mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://cdn.sparkfun.com/datasheets/Components/Switches/N301102.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="-6" y2="5" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<smd name="4" x="-6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="2" x="-6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="1" x="6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="3" x="6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<text x="0" y="6.223" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-6.223" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_6.0MM_KIT" urn="urn:adsk.eagle:footprint:12047470/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 6.0mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-1000)&lt;/p&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-3.2664" y="3.142"/>
<vertex x="-3.2589" y="3.1445" curve="89.986886"/>
<vertex x="-4.1326" y="2.286"/>
<vertex x="-4.1351" y="2.2657" curve="90.00652"/>
<vertex x="-3.2563" y="1.392"/>
<vertex x="-3.2487" y="1.3869" curve="90.006616"/>
<vertex x="-2.3826" y="2.2403"/>
<vertex x="-2.3775" y="2.2683" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.2462" y="2.7026"/>
<vertex x="-3.2589" y="2.7051" curve="90.026544"/>
<vertex x="-3.6881" y="2.2733"/>
<vertex x="-3.6881" y="2.2632" curve="89.974074"/>
<vertex x="-3.2562" y="1.8213"/>
<vertex x="-3.2259" y="1.8186" curve="90.051271"/>
<vertex x="-2.8093" y="2.2658"/>
<vertex x="-2.8093" y="2.2606" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="3.2411" y="3.1395"/>
<vertex x="3.2486" y="3.142" curve="89.986886"/>
<vertex x="2.3749" y="2.2835"/>
<vertex x="2.3724" y="2.2632" curve="90.00652"/>
<vertex x="3.2512" y="1.3895"/>
<vertex x="3.2588" y="1.3844" curve="90.006616"/>
<vertex x="4.1249" y="2.2378"/>
<vertex x="4.13" y="2.2658" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.2613" y="2.7001"/>
<vertex x="3.2486" y="2.7026" curve="90.026544"/>
<vertex x="2.8194" y="2.2708"/>
<vertex x="2.8194" y="2.2607" curve="89.974074"/>
<vertex x="3.2513" y="1.8188"/>
<vertex x="3.2816" y="1.8161" curve="90.051271"/>
<vertex x="3.6982" y="2.2633"/>
<vertex x="3.6982" y="2.2581" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.2613" y="-1.3868"/>
<vertex x="-3.2538" y="-1.3843" curve="89.986886"/>
<vertex x="-4.1275" y="-2.2428"/>
<vertex x="-4.13" y="-2.2631" curve="90.00652"/>
<vertex x="-3.2512" y="-3.1368"/>
<vertex x="-3.2436" y="-3.1419" curve="90.006616"/>
<vertex x="-2.3775" y="-2.2885"/>
<vertex x="-2.3724" y="-2.2605" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.2411" y="-1.8262"/>
<vertex x="-3.2538" y="-1.8237" curve="90.026544"/>
<vertex x="-3.683" y="-2.2555"/>
<vertex x="-3.683" y="-2.2656" curve="89.974074"/>
<vertex x="-3.2511" y="-2.7075"/>
<vertex x="-3.2208" y="-2.7102" curve="90.051271"/>
<vertex x="-2.8042" y="-2.263"/>
<vertex x="-2.8042" y="-2.2682" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="3.2411" y="-1.3843"/>
<vertex x="3.2486" y="-1.3818" curve="89.986886"/>
<vertex x="2.3749" y="-2.2403"/>
<vertex x="2.3724" y="-2.2606" curve="90.00652"/>
<vertex x="3.2512" y="-3.1343"/>
<vertex x="3.2588" y="-3.1394" curve="90.006616"/>
<vertex x="4.1249" y="-2.286"/>
<vertex x="4.13" y="-2.258" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.2613" y="-1.8237"/>
<vertex x="3.2486" y="-1.8212" curve="90.026544"/>
<vertex x="2.8194" y="-2.253"/>
<vertex x="2.8194" y="-2.2631" curve="89.974074"/>
<vertex x="3.2513" y="-2.705"/>
<vertex x="3.2816" y="-2.7077" curve="90.051271"/>
<vertex x="3.6982" y="-2.2605"/>
<vertex x="3.6982" y="-2.2657" curve="90.012964"/>
</polygon>
<text x="0" y="3.175" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_5.2MM" urn="urn:adsk.eagle:footprint:12047471/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 5.2mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/Buttons/SMD-Button.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-1.54" y1="-2.54" x2="-2.54" y2="-1.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-1.24" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.54" x2="-1.54" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.54" y1="2.54" x2="1.54" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.54" y1="2.54" x2="2.54" y2="1.54" width="0.2032" layer="51"/>
<wire x1="2.54" y1="1.24" x2="2.54" y2="-1.24" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.54" x2="1.54" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="1.54" y1="-2.54" x2="-1.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-2.794" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="2.794" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="3" x="-2.794" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="4" x="2.794" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<text x="0" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_RIGHT_ANGLE" urn="urn:adsk.eagle:footprint:12047472/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;</description>
<hole x="0" y="0.9" drill="0.7"/>
<hole x="0" y="-0.9" drill="0.7"/>
<smd name="1" x="-1.95" y="0" dx="2" dy="1.1" layer="1" rot="R90"/>
<smd name="2" x="1.95" y="0" dx="2" dy="1.1" layer="1" rot="R90"/>
<wire x1="-2" y1="1.2" x2="-2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.5" x2="2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="2" y1="1.5" x2="2" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.2" x2="-2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.5" x2="-0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-0.7" y1="-1.5" x2="0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="0.7" y1="-1.5" x2="2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.5" x2="2" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-0.7" y1="-2.1" x2="0.7" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="0.7" y1="-2.1" x2="0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-0.7" y1="-2.1" x2="-0.7" y2="-1.5" width="0.2032" layer="21"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_4.6X2.8MM" urn="urn:adsk.eagle:footprint:12047473/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 4.6 x 2.8mm&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ck-components.com/media/1479/kmr2.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<smd name="3" x="2.05" y="0.8" dx="0.9" dy="1" layer="1"/>
<smd name="2" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1"/>
<smd name="1" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1"/>
<smd name="4" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1"/>
<wire x1="-2.1" y1="1.4" x2="-2.1" y2="-1.4" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.4" x2="2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.4" x2="2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
<wire x1="1.338" y1="-1.4" x2="-1.338" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-1.338" y1="1.4" x2="1.338" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="0.13" x2="-2.1" y2="-0.13" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-0.13" x2="2.1" y2="0.13" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.805" width="0.127" layer="21"/>
<rectangle x1="-2.3" y1="0.5" x2="-2.1" y2="1.1" layer="51"/>
<rectangle x1="-2.3" y1="-1.1" x2="-2.1" y2="-0.5" layer="51"/>
<rectangle x1="2.1" y1="-1.1" x2="2.3" y2="-0.5" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="0.5" x2="2.3" y2="1.1" layer="51" rot="R180"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_RIGHT_ANGLE" urn="urn:adsk.eagle:footprint:12047487/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://cdn.sparkfun.com/datasheets/Components/Switches/SW016.JPG"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="1.5" y1="-3.8" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="3.5" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="3.5" x2="3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.65" y1="3.5" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="3" y2="2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="2" x2="3" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.777" y1="1" x2="-3.777" y2="-2.445003125" width="0.2032" layer="21"/>
<wire x1="-3.777" y1="-2.445003125" x2="3.777" y2="-2.445003125" width="0.2032" layer="21"/>
<wire x1="3.777" y1="-2.445003125" x2="3.777" y2="1" width="0.2032" layer="21"/>
<wire x1="2" y1="2.127" x2="-2" y2="2.127" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-2.445003125" x2="0.635" y2="-2.445003125" width="0.4064" layer="51"/>
<wire x1="3.777" y1="1.905" x2="3.777" y2="3.683" width="0.4064" layer="51"/>
<wire x1="-3.777" y1="1.905" x2="-3.777" y2="3.683" width="0.4064" layer="51"/>
<wire x1="0" y1="-0.635" x2="0" y2="-1.651" width="0.127" layer="51"/>
<wire x1="0" y1="-1.651" x2="-0.127" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-0.127" y1="-1.27" x2="0.127" y2="-1.27" width="0.127" layer="51"/>
<wire x1="0.127" y1="-1.27" x2="0" y2="-1.651" width="0.127" layer="51"/>
<wire x1="1.905" y1="3.683" x2="2.921" y2="3.683" width="0.127" layer="51"/>
<wire x1="2.921" y1="3.683" x2="2.54" y2="3.556" width="0.127" layer="51"/>
<wire x1="2.54" y1="3.556" x2="2.54" y2="3.81" width="0.127" layer="51"/>
<wire x1="2.54" y1="3.81" x2="2.921" y2="3.683" width="0.127" layer="51"/>
<wire x1="-1.905" y1="3.683" x2="-2.921" y2="3.683" width="0.127" layer="51"/>
<wire x1="-2.921" y1="3.683" x2="-2.54" y2="3.81" width="0.127" layer="51"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="3.556" width="0.127" layer="51"/>
<wire x1="-2.54" y1="3.556" x2="-2.921" y2="3.683" width="0.127" layer="51"/>
<pad name="ANCHOR1" x="-3.7032" y="2.5" drill="1.2" diameter="2.2"/>
<pad name="ANCHOR2" x="3.7032" y="2.5" drill="1.2" diameter="2.2"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<circle x="2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="-2.5" y="0" radius="0.889" width="0" layer="30"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<text x="-1.27" y="-0.254" size="0.8128" layer="51" font="vector" ratio="15">Foot</text>
<text x="-1.27" y="3.302" size="0.8128" layer="51" font="vector" ratio="15">Feet</text>
</package>
</packages>
<packages3d>
<package3d name="TACTILE_SWITCH_PTH_6.0MM" urn="urn:adsk.eagle:package:12047493/6" type="model" library_version="1" library_locally_modified="yes">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 6.0mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-1000)&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_PTH_6.0MM"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_SMD_4.5MM" urn="urn:adsk.eagle:package:12047492/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 4.5mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://spec_sheets.e-switch.com/specs/P010338.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_SMD_4.5MM"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_PTH_12MM" urn="urn:adsk.eagle:package:12047491/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 12mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-5050)&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_PTH_12MM"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_SMD_6.0X3.5MM" urn="urn:adsk.eagle:package:12047490/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 6.0 x 3.5 mm&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/1101.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_SMD_6.0X3.5MM"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_SMD_6.2MM_TALL" urn="urn:adsk.eagle:package:12047519/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 6.2mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.apem.com/files/apem/brochures/ADTS6-ADTSM-KTSC6.pdf"&gt;Datasheet&lt;/a&gt; (ADTSM63NVTR)&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_SMD_6.2MM_TALL"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_PTH_RIGHT_ANGLE_KIT" urn="urn:adsk.eagle:package:12047521/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://cdn.sparkfun.com/datasheets/Components/Switches/SW016.JPG"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_PTH_RIGHT_ANGLE_KIT"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_SMD_12MM" urn="urn:adsk.eagle:package:12047522/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 12mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://cdn.sparkfun.com/datasheets/Components/Switches/N301102.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_SMD_12MM"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_PTH_6.0MM_KIT" urn="urn:adsk.eagle:package:12047523/2" type="model" library_version="1" library_locally_modified="yes">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 6.0mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-1000)&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_PTH_6.0MM_KIT"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_SMD_5.2MM" urn="urn:adsk.eagle:package:12047524/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 5.2mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/Buttons/SMD-Button.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_SMD_5.2MM"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_SMD_RIGHT_ANGLE" urn="urn:adsk.eagle:package:12047525/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_SMD_RIGHT_ANGLE"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_SMD_4.6X2.8MM" urn="urn:adsk.eagle:package:12047526/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 4.6 x 2.8mm&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ck-components.com/media/1479/kmr2.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_SMD_4.6X2.8MM"/>
</packageinstances>
</package3d>
<package3d name="TACTILE_SWITCH_PTH_RIGHT_ANGLE" urn="urn:adsk.eagle:package:12047540/1" type="box" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://cdn.sparkfun.com/datasheets/Components/Switches/SW016.JPG"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_PTH_RIGHT_ANGLE"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SWITCH-MOMENTARY-2" urn="urn:adsk.eagle:symbol:12047423/1" library_version="1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;</description>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.508" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOMENTARY-SWITCH-SPST" urn="urn:adsk.eagle:component:12047551/1" locally_modified="yes" prefix="S" library_version="1" library_locally_modified="yes">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;h4&gt;Variants&lt;/h4&gt;
&lt;h5&gt;PTH-12MM - 12mm square, through-hole&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9190"&gt;Momentary Pushbutton Switch - 12mm Square&lt;/a&gt; (COM-09190)&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;PTH-6.0MM, PTH-6.0MM-KIT - 6.0mm square, through-hole&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/97"&gt;Mini Pushbutton Switch&lt;/a&gt; (COM-00097)&lt;/li&gt;
&lt;li&gt;KIT package intended for soldering kit's - only one side of pads' copper is exposed.&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;PTH-RIGHT-ANGLE-KIT - Right-angle, through-hole&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10791"&gt;Right Angle Tactile Button&lt;/a&gt; - Used on &lt;a href="https://www.sparkfun.com/products/11734"&gt;
SparkFun BigTime Watch Kit&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-12MM - 12mm square, surface-mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12993"&gt;Tactile Button - SMD (12mm)&lt;/a&gt; (COM-12993)&lt;/li&gt;
&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/11888"&gt;SparkFun PicoBoard&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-4.5MM - 4.5mm Square Trackball Switch&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/13169"&gt;SparkFun Blackberry Trackballer Breakout&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-4.6MMX2.8MM -  4.60mm x 2.80mm, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/13664"&gt;SparkFun SAMD21 Mini Breakout&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-5.2MM, SMD-5.2-REDUNDANT - 5.2mm square, surface-mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8720"&gt;Mini Pushbutton Switch - SMD&lt;/a&gt; (COM-08720)&lt;/li&gt;
&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/11114"&gt;Arduino Pro Mini&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;REDUNDANT package connects both switch circuits together&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-6.0X3.5MM - 6.0 x 3.5mm, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8229"&gt;Momentary Reset Switch SMD&lt;/a&gt; (COM-08229)&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-6.2MM-TALL - 6.2mm square, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12992"&gt;Tactile Button - SMD (6mm)&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/12651"&gt;SparkFun Digital Sandbox&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-RIGHT-ANGLE - Right-angle, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/13036"&gt;SparkFun Block for Intel® Edison - Arduino&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="SWITCH-MOMENTARY-2" x="0" y="0"/>
</gates>
<devices>
<device name="-PTH-6.0MM" package="TACTILE_SWITCH_PTH_6.0MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047493/6"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value=" SWCH-08441"/>
<attribute name="SF_SKU" value="COM-00097"/>
</technology>
</technologies>
</device>
<device name="-SMD-4.5MM" package="TACTILE_SWITCH_SMD_4.5MM">
<connects>
<connect gate="G$1" pin="1" pad="2 3"/>
<connect gate="G$1" pin="2" pad="1 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047492/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-09213"/>
</technology>
</technologies>
</device>
<device name="-PTH-12MM" package="TACTILE_SWITCH_PTH_12MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047491/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-09185"/>
<attribute name="SF_SKU" value="COM-09190"/>
</technology>
</technologies>
</device>
<device name="-SMD-6.0X3.5MM" package="TACTILE_SWITCH_SMD_6.0X3.5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047490/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-00815"/>
<attribute name="SF_SKU" value="COM-08229"/>
</technology>
</technologies>
</device>
<device name="-SMD-6.2MM-TALL" package="TACTILE_SWITCH_SMD_6.2MM_TALL">
<connects>
<connect gate="G$1" pin="1" pad="A2"/>
<connect gate="G$1" pin="2" pad="B2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047519/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-11966"/>
<attribute name="SF_SKU" value="COM-12992"/>
</technology>
</technologies>
</device>
<device name="-PTH-RIGHT-ANGLE-KIT" package="TACTILE_SWITCH_PTH_RIGHT_ANGLE_KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047521/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10672"/>
<attribute name="SF_SKU" value="COM-10791"/>
</technology>
</technologies>
</device>
<device name="-SMD-12MM" package="TACTILE_SWITCH_SMD_12MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047522/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-11967"/>
<attribute name="SF_SKU" value="COM-12993"/>
</technology>
</technologies>
</device>
<device name="-PTH-6.0MM-KIT" package="TACTILE_SWITCH_PTH_6.0MM_KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047523/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08441"/>
<attribute name="SF_SKU" value="COM-00097 "/>
</technology>
</technologies>
</device>
<device name="-SMD-5.2MM" package="TACTILE_SWITCH_SMD_5.2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047524/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08247"/>
<attribute name="SF_SKU" value="COM-08720"/>
</technology>
</technologies>
</device>
<device name="-SMD-5.2-REDUNDANT" package="TACTILE_SWITCH_SMD_5.2MM">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047524/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08247"/>
<attribute name="SF_SKU" value="COM-08720"/>
</technology>
</technologies>
</device>
<device name="-SMD-RIGHT-ANGLE" package="TACTILE_SWITCH_SMD_RIGHT_ANGLE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047525/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="COMP-12265" constant="no"/>
</technology>
</technologies>
</device>
<device name="-SMD-4.6X2.8MM" package="TACTILE_SWITCH_SMD_4.6X2.8MM">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047526/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-13065"/>
</technology>
</technologies>
</device>
<device name="-PTH-RIGHT-ANGLE" package="TACTILE_SWITCH_PTH_RIGHT_ANGLE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047540/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10672" constant="no"/>
<attribute name="SF_SKU" value="COM-10791" constant="no"/>
</technology>
</technologies>
</device>
<device name="-SMD-5.2-TALL-REDUNDANT" package="TACTILE_SWITCH_SMD_5.2MM">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047524/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-14139"/>
</technology>
</technologies>
</device>
<device name="-SMD-5.2MM-TALL" package="TACTILE_SWITCH_SMD_5.2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12047524/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-14139"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X20" urn="urn:adsk.eagle:footprint:22315/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-25.4" y1="-1.905" x2="-24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="-2.54" x2="-23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="-2.54" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="-2.54" x2="-20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="-2.54" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-2.54" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-2.54" x2="-15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-2.54" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="-1.905" x2="-25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="1.905" x2="-24.765" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="2.54" x2="-23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="2.54" x2="-22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="2.54" x2="-20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="2.54" x2="-17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="2.54" x2="-15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="25.4" y1="1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-24.13" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="1.27" drill="1.016" shape="octagon"/>
<text x="-25.4" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-25.4" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-24.384" y1="-1.524" x2="-23.876" y2="-1.016" layer="51"/>
<rectangle x1="-24.384" y1="1.016" x2="-23.876" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="51"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="51"/>
<rectangle x1="23.876" y1="1.016" x2="24.384" y2="1.524" layer="51"/>
<rectangle x1="23.876" y1="-1.524" x2="24.384" y2="-1.016" layer="51"/>
</package>
<package name="2X20/90" urn="urn:adsk.eagle:footprint:22316/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-25.4" y1="-1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="0.635" x2="-25.4" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="0.635" x2="-25.4" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-24.13" y1="6.985" x2="-24.13" y2="1.27" width="0.762" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-21.59" y1="6.985" x2="-21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-19.05" y1="6.985" x2="-19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-16.51" y1="6.985" x2="-16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="6.985" x2="13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="17.78" y1="0.635" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="16.51" y1="6.985" x2="16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="20.32" y1="0.635" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="19.05" y1="6.985" x2="19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="22.86" y1="0.635" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="21.59" y1="6.985" x2="21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="25.4" y2="0.635" width="0.1524" layer="21"/>
<wire x1="25.4" y1="0.635" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="24.13" y1="6.985" x2="24.13" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-24.13" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-24.13" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-26.035" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="27.305" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-24.511" y1="0.635" x2="-23.749" y2="1.143" layer="21"/>
<rectangle x1="-21.971" y1="0.635" x2="-21.209" y2="1.143" layer="21"/>
<rectangle x1="-19.431" y1="0.635" x2="-18.669" y2="1.143" layer="21"/>
<rectangle x1="-16.891" y1="0.635" x2="-16.129" y2="1.143" layer="21"/>
<rectangle x1="-14.351" y1="0.635" x2="-13.589" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="0.635" x2="14.351" y2="1.143" layer="21"/>
<rectangle x1="16.129" y1="0.635" x2="16.891" y2="1.143" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.431" y2="1.143" layer="21"/>
<rectangle x1="21.209" y1="0.635" x2="21.971" y2="1.143" layer="21"/>
<rectangle x1="23.749" y1="0.635" x2="24.511" y2="1.143" layer="21"/>
<rectangle x1="-24.511" y1="-2.921" x2="-23.749" y2="-1.905" layer="21"/>
<rectangle x1="-21.971" y1="-2.921" x2="-21.209" y2="-1.905" layer="21"/>
<rectangle x1="-24.511" y1="-5.461" x2="-23.749" y2="-4.699" layer="21"/>
<rectangle x1="-24.511" y1="-4.699" x2="-23.749" y2="-2.921" layer="51"/>
<rectangle x1="-21.971" y1="-4.699" x2="-21.209" y2="-2.921" layer="51"/>
<rectangle x1="-21.971" y1="-5.461" x2="-21.209" y2="-4.699" layer="21"/>
<rectangle x1="-19.431" y1="-2.921" x2="-18.669" y2="-1.905" layer="21"/>
<rectangle x1="-16.891" y1="-2.921" x2="-16.129" y2="-1.905" layer="21"/>
<rectangle x1="-19.431" y1="-5.461" x2="-18.669" y2="-4.699" layer="21"/>
<rectangle x1="-19.431" y1="-4.699" x2="-18.669" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-4.699" x2="-16.129" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-5.461" x2="-16.129" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-2.921" x2="-13.589" y2="-1.905" layer="21"/>
<rectangle x1="-14.351" y1="-5.461" x2="-13.589" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-4.699" x2="-13.589" y2="-2.921" layer="51"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-11.811" y1="-5.461" x2="-11.049" y2="-4.699" layer="21"/>
<rectangle x1="-11.811" y1="-4.699" x2="-11.049" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-4.699" x2="-8.509" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-5.461" x2="-8.509" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-4.699" x2="9.271" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-5.461" x2="9.271" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-5.461" x2="11.811" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-4.699" x2="11.811" y2="-2.921" layer="51"/>
<rectangle x1="13.589" y1="-2.921" x2="14.351" y2="-1.905" layer="21"/>
<rectangle x1="16.129" y1="-2.921" x2="16.891" y2="-1.905" layer="21"/>
<rectangle x1="13.589" y1="-5.461" x2="14.351" y2="-4.699" layer="21"/>
<rectangle x1="13.589" y1="-4.699" x2="14.351" y2="-2.921" layer="51"/>
<rectangle x1="16.129" y1="-4.699" x2="16.891" y2="-2.921" layer="51"/>
<rectangle x1="16.129" y1="-5.461" x2="16.891" y2="-4.699" layer="21"/>
<rectangle x1="18.669" y1="-2.921" x2="19.431" y2="-1.905" layer="21"/>
<rectangle x1="21.209" y1="-2.921" x2="21.971" y2="-1.905" layer="21"/>
<rectangle x1="18.669" y1="-5.461" x2="19.431" y2="-4.699" layer="21"/>
<rectangle x1="18.669" y1="-4.699" x2="19.431" y2="-2.921" layer="51"/>
<rectangle x1="21.209" y1="-4.699" x2="21.971" y2="-2.921" layer="51"/>
<rectangle x1="21.209" y1="-5.461" x2="21.971" y2="-4.699" layer="21"/>
<rectangle x1="23.749" y1="-2.921" x2="24.511" y2="-1.905" layer="21"/>
<rectangle x1="23.749" y1="-5.461" x2="24.511" y2="-4.699" layer="21"/>
<rectangle x1="23.749" y1="-4.699" x2="24.511" y2="-2.921" layer="51"/>
</package>
<package name="2X12" urn="urn:adsk.eagle:footprint:22276/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-2.54" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="-2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="20" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="22" x="11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="23" x="13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="24" x="13.97" y="1.27" drill="1.016" shape="octagon"/>
<text x="-15.24" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-15.24" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
</package>
<package name="2X12/90" urn="urn:adsk.eagle:footprint:22277/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-15.24" y1="-1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="6.985" x2="13.97" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="14" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="16" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="18" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="20" x="8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="22" x="11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="24" x="13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="13" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="15" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="17" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="19" x="8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="21" x="11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="23" x="13.97" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-15.875" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="17.145" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-14.351" y1="0.635" x2="-13.589" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="0.635" x2="14.351" y2="1.143" layer="21"/>
<rectangle x1="-14.351" y1="-2.921" x2="-13.589" y2="-1.905" layer="21"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-14.351" y1="-5.461" x2="-13.589" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-4.699" x2="-13.589" y2="-2.921" layer="51"/>
<rectangle x1="-11.811" y1="-4.699" x2="-11.049" y2="-2.921" layer="51"/>
<rectangle x1="-11.811" y1="-5.461" x2="-11.049" y2="-4.699" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-5.461" x2="-8.509" y2="-4.699" layer="21"/>
<rectangle x1="-9.271" y1="-4.699" x2="-8.509" y2="-2.921" layer="51"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-4.699" x2="9.271" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-5.461" x2="9.271" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
<rectangle x1="13.589" y1="-2.921" x2="14.351" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-5.461" x2="11.811" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-4.699" x2="11.811" y2="-2.921" layer="51"/>
<rectangle x1="13.589" y1="-4.699" x2="14.351" y2="-2.921" layer="51"/>
<rectangle x1="13.589" y1="-5.461" x2="14.351" y2="-4.699" layer="21"/>
</package>
<package name="2X04" urn="urn:adsk.eagle:footprint:22351/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
</package>
<package name="2X04/90" urn="urn:adsk.eagle:footprint:22352/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-5.715" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.985" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
</package>
<package name="1X06" urn="urn:adsk.eagle:footprint:22361/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-7.6962" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
<package name="1X06/90" urn="urn:adsk.eagle:footprint:22362/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-8.255" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="9.525" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="2X20" urn="urn:adsk.eagle:package:22443/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X20"/>
</packageinstances>
</package3d>
<package3d name="2X20/90" urn="urn:adsk.eagle:package:22440/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X20/90"/>
</packageinstances>
</package3d>
<package3d name="2X12" urn="urn:adsk.eagle:package:22420/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X12"/>
</packageinstances>
</package3d>
<package3d name="2X12/90" urn="urn:adsk.eagle:package:22422/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X12/90"/>
</packageinstances>
</package3d>
<package3d name="2X04" urn="urn:adsk.eagle:package:22461/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X04"/>
</packageinstances>
</package3d>
<package3d name="2X04/90" urn="urn:adsk.eagle:package:22465/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X04/90"/>
</packageinstances>
</package3d>
<package3d name="1X06" urn="urn:adsk.eagle:package:22472/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X06"/>
</packageinstances>
</package3d>
<package3d name="1X06/90" urn="urn:adsk.eagle:package:22475/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X06/90"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINH2X20" urn="urn:adsk.eagle:symbol:22314/1" library_version="3">
<wire x1="-6.35" y1="-27.94" x2="8.89" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-27.94" x2="8.89" y2="25.4" width="0.4064" layer="94"/>
<wire x1="8.89" y1="25.4" x2="-6.35" y2="25.4" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="25.4" x2="-6.35" y2="-27.94" width="0.4064" layer="94"/>
<text x="-6.35" y="26.035" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="27" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="28" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="29" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="30" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="31" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="32" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="33" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="34" x="5.08" y="-17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="35" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="36" x="5.08" y="-20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="37" x="-2.54" y="-22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="38" x="5.08" y="-22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="39" x="-2.54" y="-25.4" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="40" x="5.08" y="-25.4" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINH2X12" urn="urn:adsk.eagle:symbol:22275/1" library_version="4">
<wire x1="-6.35" y1="-17.78" x2="8.89" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-17.78" x2="8.89" y2="15.24" width="0.4064" layer="94"/>
<wire x1="8.89" y1="15.24" x2="-6.35" y2="15.24" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="15.24" x2="-6.35" y2="-17.78" width="0.4064" layer="94"/>
<text x="-6.35" y="15.875" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINH2X4" urn="urn:adsk.eagle:symbol:22350/1" library_version="4">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINHD6" urn="urn:adsk.eagle:symbol:22360/1" library_version="4">
<wire x1="-6.35" y1="-7.62" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="10.16" x2="-6.35" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X20" urn="urn:adsk.eagle:component:22518/4" prefix="JP" uservalue="yes" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X20" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X20">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22443/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="12" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="2X20/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22440/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X12" urn="urn:adsk.eagle:component:22506/4" prefix="JP" uservalue="yes" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X12" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X12">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22420/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="2X12/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22422/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X4" urn="urn:adsk.eagle:component:22527/4" prefix="JP" uservalue="yes" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X04">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22461/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="47" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="2X04/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22465/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="8" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X6" urn="urn:adsk.eagle:component:22533/4" prefix="JP" uservalue="yes" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD6" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="1X06">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22472/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="79" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="1X06/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22475/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="10" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Boards" urn="urn:adsk.eagle:library:12590577">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
This library contains footprints for SparkFun breakout boards, microcontrollers (Arduino, Particle, Teensy, etc.),  breadboards, non-RF modules, etc.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SPARKFUN_PRO_MICRO" urn="urn:adsk.eagle:footprint:12590637/1" library_version="1">
<description>&lt;h3&gt;SparkFun Pro Mico Footprint (with USB connector)&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 24&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;li&gt;Area:0.7x1.3"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;SparkFun Pro Micro&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-8.89" y1="16.51" x2="-8.89" y2="-16.51" width="0.127" layer="51"/>
<wire x1="-8.89" y1="-16.51" x2="8.89" y2="-16.51" width="0.127" layer="51"/>
<wire x1="8.89" y1="-16.51" x2="8.89" y2="16.51" width="0.127" layer="51"/>
<wire x1="8.89" y1="16.51" x2="-8.89" y2="16.51" width="0.127" layer="51"/>
<wire x1="-3.81" y1="16.51" x2="-3.81" y2="17.78" width="0.127" layer="51"/>
<wire x1="-3.81" y1="17.78" x2="3.81" y2="17.78" width="0.127" layer="51"/>
<wire x1="3.81" y1="17.78" x2="3.81" y2="16.51" width="0.127" layer="51"/>
<pad name="1" x="-7.62" y="12.7" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-7.62" y="10.16" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-7.62" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="4" x="-7.62" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="5" x="-7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="6" x="-7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="7" x="-7.62" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="8" x="-7.62" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="9" x="-7.62" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="10" x="-7.62" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="11" x="-7.62" y="-12.7" drill="1.016" diameter="1.8796"/>
<pad name="12" x="-7.62" y="-15.24" drill="1.016" diameter="1.8796"/>
<pad name="13" x="7.62" y="-15.24" drill="1.016" diameter="1.8796"/>
<pad name="14" x="7.62" y="-12.7" drill="1.016" diameter="1.8796"/>
<pad name="15" x="7.62" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="16" x="7.62" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="17" x="7.62" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="18" x="7.62" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="19" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="20" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="21" x="7.62" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="22" x="7.62" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="23" x="7.62" y="10.16" drill="1.016" diameter="1.8796"/>
<pad name="24" x="7.62" y="12.7" drill="1.016" diameter="1.8796"/>
<text x="0" y="18.034" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-16.764" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<text x="-1.27" y="16.51" size="0.8128" layer="51" font="vector" ratio="20">USB</text>
</package>
</packages>
<packages3d>
<package3d name="SPARKFUN_PRO_MICRO" urn="urn:adsk.eagle:package:12590700/1" type="box" library_version="1">
<description>&lt;h3&gt;SparkFun Pro Mico Footprint (with USB connector)&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 24&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;li&gt;Area:0.7x1.3"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;SparkFun Pro Micro&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SPARKFUN_PRO_MICRO"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SPARKFUN_PRO_MICRO" urn="urn:adsk.eagle:symbol:12590598/1" library_version="1">
<description>&lt;h3&gt;SparkFun Pro Micro&lt;/h3&gt;
&lt;p&gt;3.3V/5V at 8MHz/16MHz. Does not require a USB to serial converter board for programming.  &lt;/p&gt;</description>
<wire x1="-7.62" y1="17.78" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="17.78" x2="-7.62" y2="17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<text x="-7.62" y="18.542" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="-10.16" y="5.08" length="short"/>
<pin name="*3" x="-10.16" y="2.54" length="short"/>
<pin name="4" x="-10.16" y="0" length="short"/>
<pin name="*5" x="-10.16" y="-2.54" length="short"/>
<pin name="*6" x="-10.16" y="-5.08" length="short"/>
<pin name="7" x="-10.16" y="-7.62" length="short"/>
<pin name="8" x="-10.16" y="-10.16" length="short"/>
<pin name="*9" x="-10.16" y="-12.7" length="short"/>
<pin name="*10" x="12.7" y="-12.7" length="short" rot="R180"/>
<pin name="11(MOSI)" x="12.7" y="-10.16" length="short" rot="R180"/>
<pin name="12(MISO)" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="13(SCK)" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="A0" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="A1" x="12.7" y="0" length="short" rot="R180"/>
<pin name="A2" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="A3" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="GND" x="-10.16" y="7.62" length="short" direction="pwr"/>
<pin name="GND@2" x="12.7" y="12.7" length="short" direction="pwr" rot="R180"/>
<pin name="RAW" x="12.7" y="15.24" length="short" direction="pwr" rot="R180"/>
<pin name="GND@1" x="-10.16" y="10.16" length="short" direction="pwr"/>
<pin name="RESET" x="12.7" y="10.16" length="short" rot="R180"/>
<pin name="RXI" x="-10.16" y="12.7" length="short"/>
<pin name="TXO" x="-10.16" y="15.24" length="short"/>
<pin name="VCC" x="12.7" y="7.62" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPARKFUN_PRO_MICRO" urn="urn:adsk.eagle:component:12590751/1" prefix="B" library_version="1">
<description>&lt;h3&gt;SparkFun Pro Micro &lt;/h3&gt;
&lt;p&gt;Atmega32U4 compatible footprint.&lt;/p&gt;

&lt;b&gt;&lt;p&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12587”&gt;3.3V/8MHz&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12640”&gt;5V/16MHz&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SPARKFUN_PRO_MICRO" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SPARKFUN_PRO_MICRO">
<connects>
<connect gate="G$1" pin="*10" pad="13"/>
<connect gate="G$1" pin="*3" pad="6"/>
<connect gate="G$1" pin="*5" pad="8"/>
<connect gate="G$1" pin="*6" pad="9"/>
<connect gate="G$1" pin="*9" pad="12"/>
<connect gate="G$1" pin="11(MOSI)" pad="14"/>
<connect gate="G$1" pin="12(MISO)" pad="15"/>
<connect gate="G$1" pin="13(SCK)" pad="16"/>
<connect gate="G$1" pin="2" pad="5"/>
<connect gate="G$1" pin="4" pad="7"/>
<connect gate="G$1" pin="7" pad="10"/>
<connect gate="G$1" pin="8" pad="11"/>
<connect gate="G$1" pin="A0" pad="17"/>
<connect gate="G$1" pin="A1" pad="18"/>
<connect gate="G$1" pin="A2" pad="19"/>
<connect gate="G$1" pin="A3" pad="20"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="23"/>
<connect gate="G$1" pin="RAW" pad="24"/>
<connect gate="G$1" pin="RESET" pad="22"/>
<connect gate="G$1" pin="RXI" pad="2"/>
<connect gate="G$1" pin="TXO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="21"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:12590700/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-hirose" urn="urn:adsk.eagle:library:152">
<description>&lt;b&gt;Hirose Connectors&lt;/b&gt;&lt;p&gt;
www.hirose.co.jp&lt;p&gt;
Include : &lt;br&gt;
con-hirose-df12d(3.0)60dp0.5v80.lbr from Bob Starr &amp;lt;rtzaudio@mindspring.com&amp;gt;&lt;br&gt;
con-hirose.lbr from Bob Starr &amp;lt;rtzaudio@mindspring.com&amp;gt;&lt;br&gt;</description>
<packages>
<package name="FH12-16S-1SV" urn="urn:adsk.eagle:footprint:7256/1" library_version="2">
<description>&lt;b&gt;1mm Pitch Connectors For FPC/FFC&lt;/b&gt;&lt;p&gt;
Source: &lt;a href= "http://www.hirose.co.jp/cataloge_hp/e58605370.pdf"&gt;Data sheet&lt;/a&gt;&lt;p&gt;</description>
<wire x1="8.937" y1="2.324" x2="9.0532" y2="2.324" width="0.1016" layer="51"/>
<wire x1="9.0532" y1="2.324" x2="9.0532" y2="1.4276" width="0.1016" layer="51"/>
<wire x1="9.0532" y1="1.4276" x2="9.8002" y2="1.4276" width="0.1016" layer="51"/>
<wire x1="9.8002" y1="1.4276" x2="9.8002" y2="1.1288" width="0.1016" layer="21"/>
<wire x1="9.8002" y1="1.1288" x2="9.8998" y2="1.1288" width="0.1016" layer="21"/>
<wire x1="9.8998" y1="1.1288" x2="9.8998" y2="-0.7304" width="0.1016" layer="21"/>
<wire x1="9.8998" y1="-0.7304" x2="9.3022" y2="-0.7304" width="0.1016" layer="51"/>
<wire x1="9.3022" y1="-0.7304" x2="9.3022" y2="-1.1952" width="0.1016" layer="51"/>
<wire x1="9.3022" y1="-1.1952" x2="9.1528" y2="-1.1952" width="0.1016" layer="51"/>
<wire x1="9.1528" y1="-1.1952" x2="9.1528" y2="-1.2616" width="0.1016" layer="51"/>
<wire x1="9.1528" y1="-1.2616" x2="8.3892" y2="-1.2616" width="0.1016" layer="51"/>
<wire x1="8.3892" y1="-1.2616" x2="8.3892" y2="-1.1288" width="0.1016" layer="51"/>
<wire x1="8.9204" y1="-1.3114" x2="8.9204" y2="-1.411" width="0.1016" layer="51"/>
<wire x1="8.9204" y1="-1.411" x2="9.0698" y2="-1.411" width="0.1016" layer="51"/>
<wire x1="9.0698" y1="-1.411" x2="9.0698" y2="-1.3114" width="0.1016" layer="51"/>
<wire x1="8.937" y1="2.324" x2="8.937" y2="1.6932" width="0.1016" layer="51"/>
<wire x1="8.937" y1="1.6932" x2="8.8208" y2="1.743" width="0.1016" layer="51"/>
<wire x1="8.3892" y1="-1.1288" x2="4.0018" y2="-1.1288" width="0.1016" layer="51"/>
<wire x1="4.0018" y1="-1.1288" x2="4.0018" y2="-1.2782" width="0.1016" layer="51"/>
<wire x1="4.0018" y1="-1.2782" x2="-4.0018" y2="-1.2782" width="0.1016" layer="51"/>
<wire x1="-4.0018" y1="-1.2782" x2="-4.0018" y2="0.6142" width="0.1016" layer="51"/>
<wire x1="-4" y1="0.6142" x2="4" y2="0.6142" width="0.1016" layer="51"/>
<wire x1="4.0018" y1="0.6142" x2="4.0018" y2="-1.1288" width="0.1016" layer="51"/>
<wire x1="-7.8726" y1="-1.1288" x2="-7.8726" y2="-1.2284" width="0.1016" layer="51"/>
<wire x1="-7.8726" y1="-1.2284" x2="-9.1528" y2="-1.2284" width="0.1016" layer="51"/>
<wire x1="-9.1528" y1="-1.2284" x2="-9.1528" y2="-1.1454" width="0.1016" layer="51"/>
<wire x1="-9.1528" y1="-1.1454" x2="-9.2856" y2="-1.1454" width="0.1016" layer="51"/>
<wire x1="-9.2856" y1="-1.1454" x2="-9.2856" y2="-0.6972" width="0.1016" layer="51"/>
<wire x1="-9.2856" y1="-0.6972" x2="-9.8998" y2="-0.6972" width="0.1016" layer="51"/>
<wire x1="-9.8998" y1="-0.6972" x2="-9.8998" y2="1.1786" width="0.1016" layer="21"/>
<wire x1="-9.8998" y1="1.1786" x2="-9.8002" y2="1.1786" width="0.1016" layer="21"/>
<wire x1="-9.8002" y1="1.1786" x2="-9.8002" y2="1.4774" width="0.1016" layer="21"/>
<wire x1="-9.8002" y1="1.4774" x2="-9.0532" y2="1.4774" width="0.1016" layer="51"/>
<wire x1="-9.0532" y1="1.4774" x2="-9.0532" y2="2.3904" width="0.1016" layer="51"/>
<wire x1="-9.0532" y1="2.3904" x2="-8.9204" y2="2.3904" width="0.1016" layer="51"/>
<wire x1="-8.9204" y1="2.3904" x2="-8.9204" y2="1.743" width="0.1016" layer="51"/>
<wire x1="-8.8042" y1="1.7098" x2="-8.8042" y2="1.0458" width="0.1016" layer="51"/>
<wire x1="-8.8042" y1="1.0458" x2="-8.688" y2="1.0458" width="0.1016" layer="51"/>
<wire x1="-8.688" y1="1.0458" x2="-8.688" y2="1.2784" width="0.1016" layer="51"/>
<wire x1="-8.688" y1="1.2784" x2="8.688" y2="1.2784" width="0.1016" layer="51"/>
<wire x1="8.688" y1="1.2784" x2="8.688" y2="0.9794" width="0.1016" layer="51"/>
<wire x1="8.688" y1="0.9794" x2="8.8208" y2="0.9794" width="0.1016" layer="51"/>
<wire x1="8.8208" y1="0.9794" x2="8.8208" y2="1.743" width="0.1016" layer="51"/>
<wire x1="8.8208" y1="1.743" x2="-8.9204" y2="1.743" width="0.1016" layer="51"/>
<wire x1="-4.0082" y1="-1.1288" x2="-7.8726" y2="-1.1288" width="0.1016" layer="51"/>
<wire x1="-9.0532" y1="-1.2782" x2="-9.0532" y2="-1.3778" width="0.1016" layer="51"/>
<wire x1="-9.0532" y1="-1.3778" x2="-8.9038" y2="-1.3778" width="0.1016" layer="51"/>
<wire x1="-8.9038" y1="-1.3778" x2="-8.9038" y2="-1.2782" width="0.1016" layer="51"/>
<smd name="1" x="-7.5" y="-1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="2" x="-6.5" y="1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="3" x="-5.5" y="-1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="4" x="-4.5" y="1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="5" x="-3.5" y="-1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="6" x="-2.5" y="1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="7" x="-1.5" y="-1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="8" x="-0.5" y="1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="9" x="0.5" y="-1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="10" x="1.5" y="1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="11" x="2.5" y="-1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="12" x="3.5" y="1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="13" x="4.5" y="-1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="14" x="5.5" y="1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="15" x="6.5" y="-1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="16" x="7.5" y="1.5" dx="0.6" dy="1.5" layer="1" stop="no" cream="no"/>
<text x="-10.182" y="-2.0418" size="1.016" layer="21" font="vector" rot="SR0">1</text>
<text x="-9.125" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-9.125" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-7.85" y1="-2.3" x2="-7.15" y2="-0.7" layer="29"/>
<rectangle x1="-7.75" y1="-2.2" x2="-7.25" y2="-0.8" layer="31"/>
<rectangle x1="-6.85" y1="0.7" x2="-6.15" y2="2.3" layer="29"/>
<rectangle x1="-6.75" y1="0.8" x2="-6.25" y2="2.2" layer="31"/>
<rectangle x1="-5.85" y1="-2.3" x2="-5.15" y2="-0.7" layer="29"/>
<rectangle x1="-5.75" y1="-2.2" x2="-5.25" y2="-0.8" layer="31"/>
<rectangle x1="-4.85" y1="0.7" x2="-4.15" y2="2.3" layer="29"/>
<rectangle x1="-4.75" y1="0.8" x2="-4.25" y2="2.2" layer="31"/>
<rectangle x1="-3.85" y1="-2.3" x2="-3.15" y2="-0.7" layer="29"/>
<rectangle x1="-3.75" y1="-2.2" x2="-3.25" y2="-0.8" layer="31"/>
<rectangle x1="-2.85" y1="0.7" x2="-2.15" y2="2.3" layer="29"/>
<rectangle x1="-2.75" y1="0.8" x2="-2.25" y2="2.2" layer="31"/>
<rectangle x1="-1.85" y1="-2.3" x2="-1.15" y2="-0.7" layer="29"/>
<rectangle x1="-1.75" y1="-2.2" x2="-1.25" y2="-0.8" layer="31"/>
<rectangle x1="-0.85" y1="0.7" x2="-0.15" y2="2.3" layer="29"/>
<rectangle x1="-0.75" y1="0.8" x2="-0.25" y2="2.2" layer="31"/>
<rectangle x1="0.15" y1="-2.3" x2="0.85" y2="-0.7" layer="29"/>
<rectangle x1="0.25" y1="-2.2" x2="0.75" y2="-0.8" layer="31"/>
<rectangle x1="1.15" y1="0.7" x2="1.85" y2="2.3" layer="29"/>
<rectangle x1="1.25" y1="0.8" x2="1.75" y2="2.2" layer="31"/>
<rectangle x1="2.15" y1="-2.3" x2="2.85" y2="-0.7" layer="29"/>
<rectangle x1="2.25" y1="-2.2" x2="2.75" y2="-0.8" layer="31"/>
<rectangle x1="3.15" y1="0.7" x2="3.85" y2="2.3" layer="29"/>
<rectangle x1="3.25" y1="0.8" x2="3.75" y2="2.2" layer="31"/>
<rectangle x1="4.15" y1="-2.3" x2="4.85" y2="-0.7" layer="29"/>
<rectangle x1="4.25" y1="-2.2" x2="4.75" y2="-0.8" layer="31"/>
<rectangle x1="5.15" y1="0.7" x2="5.85" y2="2.3" layer="29"/>
<rectangle x1="5.25" y1="0.8" x2="5.75" y2="2.2" layer="31"/>
<rectangle x1="6.15" y1="-2.3" x2="6.85" y2="-0.7" layer="29"/>
<rectangle x1="6.25" y1="-2.2" x2="6.75" y2="-0.8" layer="31"/>
<rectangle x1="7.15" y1="0.7" x2="7.85" y2="2.3" layer="29"/>
<rectangle x1="7.25" y1="0.8" x2="7.75" y2="2.2" layer="31"/>
<rectangle x1="-9.4" y1="0.45" x2="-8.6" y2="2.25" layer="1"/>
<rectangle x1="-9.4" y1="-2.25" x2="-8.6" y2="-0.45" layer="1"/>
<rectangle x1="-9.475" y1="0.375" x2="-8.525" y2="2.325" layer="29"/>
<rectangle x1="-9.35" y1="0.5" x2="-8.65" y2="2.2" layer="31"/>
<rectangle x1="-9.475" y1="-2.325" x2="-8.525" y2="-0.375" layer="29"/>
<rectangle x1="-9.35" y1="-2.2" x2="-8.65" y2="-0.5" layer="31"/>
<rectangle x1="8.6" y1="-2.25" x2="9.4" y2="-0.45" layer="1" rot="R180"/>
<rectangle x1="8.6" y1="0.45" x2="9.4" y2="2.25" layer="1" rot="R180"/>
<rectangle x1="8.525" y1="-2.325" x2="9.475" y2="-0.375" layer="29" rot="R180"/>
<rectangle x1="8.65" y1="-2.2" x2="9.35" y2="-0.5" layer="31" rot="R180"/>
<rectangle x1="8.525" y1="0.375" x2="9.475" y2="2.325" layer="29" rot="R180"/>
<rectangle x1="8.65" y1="0.5" x2="9.35" y2="2.2" layer="31" rot="R180"/>
<rectangle x1="7.3746" y1="0.7802" x2="7.607" y2="1.4774" layer="51"/>
<rectangle x1="5.3746" y1="0.7802" x2="5.607" y2="1.4774" layer="51"/>
<rectangle x1="3.3746" y1="0.7802" x2="3.607" y2="1.4774" layer="51"/>
<rectangle x1="1.3746" y1="0.7802" x2="1.607" y2="1.4774" layer="51"/>
<rectangle x1="-0.6254" y1="0.7802" x2="-0.393" y2="1.4774" layer="51"/>
<rectangle x1="-2.6254" y1="0.7802" x2="-2.393" y2="1.4774" layer="51"/>
<rectangle x1="-4.6254" y1="0.7802" x2="-4.393" y2="1.4774" layer="51"/>
<rectangle x1="-6.6254" y1="0.7802" x2="-6.393" y2="1.4774" layer="51"/>
<rectangle x1="6.393" y1="-1.4774" x2="6.6254" y2="-0.7802" layer="51" rot="R180"/>
<rectangle x1="4.393" y1="-1.4774" x2="4.6254" y2="-0.7802" layer="51" rot="R180"/>
<rectangle x1="2.393" y1="-1.4774" x2="2.6254" y2="-0.7802" layer="51" rot="R180"/>
<rectangle x1="0.393" y1="-1.4774" x2="0.6254" y2="-0.7802" layer="51" rot="R180"/>
<rectangle x1="-1.607" y1="-1.4774" x2="-1.3746" y2="-0.7802" layer="51" rot="R180"/>
<rectangle x1="-3.607" y1="-1.4774" x2="-3.3746" y2="-0.7802" layer="51" rot="R180"/>
<rectangle x1="-5.607" y1="-1.4774" x2="-5.3746" y2="-0.7802" layer="51" rot="R180"/>
<rectangle x1="-7.607" y1="-1.4774" x2="-7.3746" y2="-0.7802" layer="51" rot="R180"/>
<rectangle x1="7.4078" y1="1.7264" x2="7.5738" y2="2.2244" layer="51"/>
<rectangle x1="5.4078" y1="1.7264" x2="5.5738" y2="2.2244" layer="51"/>
<rectangle x1="3.4078" y1="1.7264" x2="3.5738" y2="2.2244" layer="51"/>
<rectangle x1="1.4078" y1="1.7264" x2="1.5738" y2="2.2244" layer="51"/>
<rectangle x1="-0.5922" y1="1.7264" x2="-0.4262" y2="2.2244" layer="51"/>
<rectangle x1="-2.5922" y1="1.7264" x2="-2.4262" y2="2.2244" layer="51"/>
<rectangle x1="-4.5922" y1="1.7264" x2="-4.4262" y2="2.2244" layer="51"/>
<rectangle x1="-6.5922" y1="1.7264" x2="-6.4262" y2="2.2244" layer="51"/>
</package>
<package name="FH12-16S-1SH" urn="urn:adsk.eagle:footprint:7257/1" library_version="2">
<description>&lt;b&gt;1mm Pitch Connectors For FPC/FFC&lt;/b&gt;&lt;p&gt;
Source: &lt;a href= "http://www.hirose.co.jp/cataloge_hp/e58605370.pdf"&gt;Data sheet&lt;/a&gt;&lt;p&gt;</description>
<wire x1="-9.7" y1="0.5" x2="-10.2" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-10.2" y1="0.5" x2="-10.2" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="-10.2" y1="-0.5" x2="-9.7" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="-9.7" y1="-0.5" x2="-9.7" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-9.7" y1="-1.9" x2="-9.4" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-9.4" y1="-1.9" x2="-9.4" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-9.4" y1="-2.2" x2="-9.7" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-9.7" y1="-2.2" x2="-9.7" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="9.7" y1="-2.9" x2="9.7" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="9.7" y1="-2.2" x2="9.4" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="9.4" y1="-2.2" x2="9.4" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="9.4" y1="-1.9" x2="9.7" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="9.7" y1="-1.9" x2="9.7" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="9.7" y1="-0.5" x2="10.2" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="10.2" y1="-0.5" x2="10.2" y2="0.5" width="0.2032" layer="51"/>
<wire x1="10.2" y1="0.5" x2="9.7" y2="0.5" width="0.2032" layer="51"/>
<wire x1="9.7" y1="0.5" x2="9.7" y2="2.5" width="0.2032" layer="51"/>
<wire x1="9.7" y1="2.5" x2="-9.7" y2="2.5" width="0.2032" layer="51"/>
<wire x1="-9.7" y1="2.5" x2="-9.7" y2="0.5" width="0.2032" layer="51"/>
<wire x1="9.4" y1="-1.9" x2="-9.4" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="9.7" y1="-2.9" x2="-9.7" y2="-2.9" width="0.2032" layer="21"/>
<wire x1="-9.7" y1="2.5" x2="-9.7" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-8" y1="2.5" x2="-9.7" y2="2.5" width="0.2032" layer="21"/>
<wire x1="9.7" y1="1.5" x2="9.7" y2="2.5" width="0.2032" layer="21"/>
<wire x1="9.7" y1="2.5" x2="8" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-9.7" y1="-1.5" x2="-9.7" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="9.7" y1="-1.9" x2="9.7" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-7.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="2" x="-6.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="3" x="-5.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="4" x="-4.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="5" x="-3.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="6" x="-2.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="7" x="-1.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="8" x="-0.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="9" x="0.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="10" x="1.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="11" x="2.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="12" x="3.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="13" x="4.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="14" x="5.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="15" x="6.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<smd name="16" x="7.5" y="3.25" dx="0.3" dy="1.3" layer="1" stop="no" cream="no"/>
<text x="-9.1976" y="4.095" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.9976" y="-0.3258" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-7.7" y1="2.55" x2="-7.3" y2="3.95" layer="29"/>
<rectangle x1="-7.625" y1="2.625" x2="-7.375" y2="3.875" layer="31"/>
<rectangle x1="-6.7" y1="2.55" x2="-6.3" y2="3.95" layer="29"/>
<rectangle x1="-6.625" y1="2.625" x2="-6.375" y2="3.875" layer="31"/>
<rectangle x1="-5.7" y1="2.55" x2="-5.3" y2="3.95" layer="29"/>
<rectangle x1="-5.625" y1="2.625" x2="-5.375" y2="3.875" layer="31"/>
<rectangle x1="-4.7" y1="2.55" x2="-4.3" y2="3.95" layer="29"/>
<rectangle x1="-4.625" y1="2.625" x2="-4.375" y2="3.875" layer="31"/>
<rectangle x1="-3.7" y1="2.55" x2="-3.3" y2="3.95" layer="29"/>
<rectangle x1="-3.625" y1="2.625" x2="-3.375" y2="3.875" layer="31"/>
<rectangle x1="-2.7" y1="2.55" x2="-2.3" y2="3.95" layer="29"/>
<rectangle x1="-2.625" y1="2.625" x2="-2.375" y2="3.875" layer="31"/>
<rectangle x1="-1.7" y1="2.55" x2="-1.3" y2="3.95" layer="29"/>
<rectangle x1="-1.625" y1="2.625" x2="-1.375" y2="3.875" layer="31"/>
<rectangle x1="-0.7" y1="2.55" x2="-0.3" y2="3.95" layer="29"/>
<rectangle x1="-0.625" y1="2.625" x2="-0.375" y2="3.875" layer="31"/>
<rectangle x1="0.3" y1="2.55" x2="0.7" y2="3.95" layer="29"/>
<rectangle x1="0.375" y1="2.625" x2="0.625" y2="3.875" layer="31"/>
<rectangle x1="1.3" y1="2.55" x2="1.7" y2="3.95" layer="29"/>
<rectangle x1="1.375" y1="2.625" x2="1.625" y2="3.875" layer="31"/>
<rectangle x1="2.3" y1="2.55" x2="2.7" y2="3.95" layer="29"/>
<rectangle x1="2.375" y1="2.625" x2="2.625" y2="3.875" layer="31"/>
<rectangle x1="3.3" y1="2.55" x2="3.7" y2="3.95" layer="29"/>
<rectangle x1="3.375" y1="2.625" x2="3.625" y2="3.875" layer="31"/>
<rectangle x1="4.3" y1="2.55" x2="4.7" y2="3.95" layer="29"/>
<rectangle x1="4.375" y1="2.625" x2="4.625" y2="3.875" layer="31"/>
<rectangle x1="5.3" y1="2.55" x2="5.7" y2="3.95" layer="29"/>
<rectangle x1="5.375" y1="2.625" x2="5.625" y2="3.875" layer="31"/>
<rectangle x1="6.3" y1="2.55" x2="6.7" y2="3.95" layer="29"/>
<rectangle x1="6.375" y1="2.625" x2="6.625" y2="3.875" layer="31"/>
<rectangle x1="7.3" y1="2.55" x2="7.7" y2="3.95" layer="29"/>
<rectangle x1="7.375" y1="2.625" x2="7.625" y2="3.875" layer="31"/>
<rectangle x1="9" y1="-1.1" x2="10.8" y2="1.1" layer="1" rot="R180"/>
<rectangle x1="8.95" y1="-1.175" x2="10.85" y2="1.175" layer="29" rot="R180"/>
<rectangle x1="9.075" y1="-1.025" x2="10.725" y2="1.025" layer="31" rot="R180"/>
<rectangle x1="-10.8" y1="-1.1" x2="-9" y2="1.1" layer="1" rot="R180"/>
<rectangle x1="-10.85" y1="-1.175" x2="-8.95" y2="1.175" layer="29" rot="R180"/>
<rectangle x1="-10.725" y1="-1.025" x2="-9.075" y2="1.025" layer="31" rot="R180"/>
<rectangle x1="-7.6" y1="2.55" x2="-7.4" y2="3.4" layer="51"/>
<rectangle x1="-6.6" y1="2.55" x2="-6.4" y2="3.4" layer="51"/>
<rectangle x1="-5.6" y1="2.55" x2="-5.4" y2="3.4" layer="51"/>
<rectangle x1="-4.6" y1="2.55" x2="-4.4" y2="3.4" layer="51"/>
<rectangle x1="-3.6" y1="2.55" x2="-3.4" y2="3.4" layer="51"/>
<rectangle x1="-2.6" y1="2.55" x2="-2.4" y2="3.4" layer="51"/>
<rectangle x1="-1.6" y1="2.55" x2="-1.4" y2="3.4" layer="51"/>
<rectangle x1="-0.6" y1="2.55" x2="-0.4" y2="3.4" layer="51"/>
<rectangle x1="0.4" y1="2.55" x2="0.6" y2="3.4" layer="51"/>
<rectangle x1="1.4" y1="2.55" x2="1.6" y2="3.4" layer="51"/>
<rectangle x1="2.4" y1="2.55" x2="2.6" y2="3.4" layer="51"/>
<rectangle x1="3.4" y1="2.55" x2="3.6" y2="3.4" layer="51"/>
<rectangle x1="4.4" y1="2.55" x2="4.6" y2="3.4" layer="51"/>
<rectangle x1="5.4" y1="2.55" x2="5.6" y2="3.4" layer="51"/>
<rectangle x1="6.4" y1="2.55" x2="6.6" y2="3.4" layer="51"/>
<rectangle x1="7.4" y1="2.55" x2="7.6" y2="3.4" layer="51"/>
<polygon width="0.2032" layer="21">
<vertex x="-7.7" y="-2.2"/>
<vertex x="-7.5" y="-2.6"/>
<vertex x="-7.3" y="-2.2"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="FH12-16S-1SV" urn="urn:adsk.eagle:package:7322/1" type="box" library_version="2">
<description>1mm Pitch Connectors For FPC/FFC
Source: Data sheet</description>
<packageinstances>
<packageinstance name="FH12-16S-1SV"/>
</packageinstances>
</package3d>
<package3d name="FH12-16S-1SH" urn="urn:adsk.eagle:package:7325/1" type="box" library_version="2">
<description>1mm Pitch Connectors For FPC/FFC
Source: Data sheet</description>
<packageinstances>
<packageinstance name="FH12-16S-1SH"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINV" urn="urn:adsk.eagle:symbol:7169/1" library_version="1">
<wire x1="0" y1="0.254" x2="0" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.254" x2="1.016" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-0.254" x2="1.27" y2="0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.254" x2="0" y2="0.254" width="0.1524" layer="94"/>
<text x="2.032" y="-0.762" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="PIN" urn="urn:adsk.eagle:symbol:7167/1" library_version="1">
<wire x1="0" y1="0.254" x2="0" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.254" x2="1.016" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-0.254" x2="1.27" y2="0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.254" x2="0" y2="0.254" width="0.1524" layer="94"/>
<text x="2.032" y="-0.762" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FH12-16S-1S" urn="urn:adsk.eagle:component:7390/2" prefix="X" library_version="2">
<description>&lt;b&gt;1mm Pitch Connectors For FPC/FFC&lt;/b&gt;&lt;p&gt;
Source: &lt;a href= "http://www.hirose.co.jp/cataloge_hp/e58605370.pdf"&gt;Data sheet&lt;/a&gt;&lt;p&gt;</description>
<gates>
<gate name="-1" symbol="PINV" x="0" y="17.78"/>
<gate name="-2" symbol="PIN" x="0" y="15.24"/>
<gate name="-3" symbol="PIN" x="0" y="12.7"/>
<gate name="-4" symbol="PIN" x="0" y="10.16"/>
<gate name="-5" symbol="PIN" x="0" y="7.62"/>
<gate name="-6" symbol="PIN" x="0" y="5.08"/>
<gate name="-7" symbol="PIN" x="0" y="2.54"/>
<gate name="-8" symbol="PIN" x="0" y="0"/>
<gate name="-9" symbol="PIN" x="0" y="-2.54"/>
<gate name="-10" symbol="PIN" x="0" y="-5.08"/>
<gate name="-11" symbol="PIN" x="0" y="-7.62"/>
<gate name="-12" symbol="PIN" x="0" y="-10.16"/>
<gate name="-13" symbol="PIN" x="0" y="-12.7"/>
<gate name="-14" symbol="PIN" x="0" y="-15.24"/>
<gate name="-15" symbol="PIN" x="0" y="-17.78"/>
<gate name="-16" symbol="PIN" x="0" y="-20.32"/>
</gates>
<devices>
<device name="V" package="FH12-16S-1SV">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-10" pin="1" pad="10"/>
<connect gate="-11" pin="1" pad="11"/>
<connect gate="-12" pin="1" pad="12"/>
<connect gate="-13" pin="1" pad="13"/>
<connect gate="-14" pin="1" pad="14"/>
<connect gate="-15" pin="1" pad="15"/>
<connect gate="-16" pin="1" pad="16"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-9" pin="1" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7322/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="H" package="FH12-16S-1SH">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-10" pin="1" pad="10"/>
<connect gate="-11" pin="1" pad="11"/>
<connect gate="-12" pin="1" pad="12"/>
<connect gate="-13" pin="1" pad="13"/>
<connect gate="-14" pin="1" pad="14"/>
<connect gate="-15" pin="1" pad="15"/>
<connect gate="-16" pin="1" pad="16"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-9" pin="1" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7325/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="oshw">
<description>&lt;p&gt;&lt;strong&gt;Open Source Hardware PCB Logos&lt;/strong&gt;
&lt;p&gt;A simple polygon based version of the OSHWA logos for use on Eagle PCB's
&lt;p&gt;For use please refer to the OSHWA definition at &lt;a href="http://www.oshwa.org/definition/"&gt;http://www.oshwa.org/definition/&lt;/a&gt;
&lt;p&gt;Based on the NBitWonder version (&lt;a href="http://nbitwonder.com"&gt;http://nbitwonder.com&lt;/a&gt;)
&lt;p&gt;&lt;strong&gt;Andrew Cooper&lt;/strong&gt;&lt;br&gt;28Nov2016&lt;br&gt;&lt;a href="http://www.darkerview.com"&gt;www.DarkerView.com&lt;/a&gt;</description>
<packages>
<package name="OSHW_16MM">
<polygon width="0.127" layer="25">
<vertex x="-0.7874" y="-2.032" curve="-159.559114"/>
<vertex x="0" y="2.1844" curve="-161.899642"/>
<vertex x="0.9144" y="-2.032"/>
<vertex x="2.2352" y="-5.3848"/>
<vertex x="2.5654" y="-5.2324"/>
<vertex x="2.9718" y="-5.0038"/>
<vertex x="3.2766" y="-4.8006"/>
<vertex x="4.953" y="-5.9944"/>
<vertex x="5.3848" y="-5.6134"/>
<vertex x="5.7912" y="-5.1816"/>
<vertex x="6.0706" y="-4.8514"/>
<vertex x="6.2484" y="-4.6228"/>
<vertex x="5.0546" y="-2.8956"/>
<vertex x="5.2578" y="-2.4638"/>
<vertex x="5.4356" y="-1.9812"/>
<vertex x="5.588" y="-1.4732"/>
<vertex x="5.6642" y="-1.1684"/>
<vertex x="7.7216" y="-0.7874"/>
<vertex x="7.7216" y="-0.3556"/>
<vertex x="7.7216" y="0.0508"/>
<vertex x="7.6962" y="0.5588"/>
<vertex x="7.6454" y="1.0922"/>
<vertex x="5.5372" y="1.4986"/>
<vertex x="5.3848" y="2.032"/>
<vertex x="5.1562" y="2.5146"/>
<vertex x="5.0038" y="2.794"/>
<vertex x="4.8514" y="3.048"/>
<vertex x="6.0452" y="4.7244"/>
<vertex x="5.7404" y="5.08"/>
<vertex x="5.461" y="5.4102"/>
<vertex x="5.08" y="5.7404"/>
<vertex x="4.6482" y="6.1214"/>
<vertex x="2.8702" y="4.9022"/>
<vertex x="2.5654" y="5.08"/>
<vertex x="2.1336" y="5.2578"/>
<vertex x="1.7272" y="5.4102"/>
<vertex x="1.3462" y="5.5372"/>
<vertex x="1.016" y="7.5692"/>
<vertex x="0.5334" y="7.62"/>
<vertex x="0.0254" y="7.62"/>
<vertex x="-0.508" y="7.62"/>
<vertex x="-0.9398" y="7.5692"/>
<vertex x="-1.0414" y="6.985"/>
<vertex x="-1.3208" y="5.5118"/>
<vertex x="-1.8034" y="5.3594"/>
<vertex x="-2.2098" y="5.207"/>
<vertex x="-2.6162" y="5.0038"/>
<vertex x="-2.921" y="4.8514"/>
<vertex x="-4.6228" y="6.0706"/>
<vertex x="-5.0038" y="5.7404"/>
<vertex x="-5.334" y="5.4356"/>
<vertex x="-5.6642" y="5.08"/>
<vertex x="-5.9944" y="4.7244"/>
<vertex x="-4.8006" y="2.9972"/>
<vertex x="-5.0546" y="2.6162"/>
<vertex x="-5.2578" y="2.1844"/>
<vertex x="-5.4102" y="1.778"/>
<vertex x="-5.5626" y="1.2446"/>
<vertex x="-7.62" y="0.9398"/>
<vertex x="-7.6454" y="0.6096"/>
<vertex x="-7.6454" y="-0.0762"/>
<vertex x="-7.6454" y="-0.5334"/>
<vertex x="-7.62" y="-0.9398"/>
<vertex x="-5.5626" y="-1.3208"/>
<vertex x="-5.461" y="-1.778"/>
<vertex x="-5.3086" y="-2.159"/>
<vertex x="-5.1054" y="-2.6162"/>
<vertex x="-4.826" y="-3.1496"/>
<vertex x="-6.0198" y="-4.8514"/>
<vertex x="-5.715" y="-5.207"/>
<vertex x="-5.4102" y="-5.5372"/>
<vertex x="-5.0038" y="-5.8928"/>
<vertex x="-4.6736" y="-6.1468"/>
<vertex x="-2.9972" y="-4.953"/>
<vertex x="-2.6416" y="-5.1562"/>
<vertex x="-2.286" y="-5.334"/>
<vertex x="-1.9812" y="-5.461"/>
</polygon>
<text x="9.779" y="-7.0612" size="2.54" layer="25" rot="R180">open source
  hardware</text>
</package>
<package name="OSHW_8MM">
<polygon width="0.0508" layer="25">
<vertex x="-0.381" y="-0.9906" curve="-160.613168"/>
<vertex x="0" y="1.0668" curve="-162.088112"/>
<vertex x="0.4572" y="-0.9906"/>
<vertex x="1.1176" y="-2.6924"/>
<vertex x="1.2954" y="-2.5908"/>
<vertex x="1.4732" y="-2.4892"/>
<vertex x="1.651" y="-2.3876"/>
<vertex x="2.4892" y="-2.9972"/>
<vertex x="2.667" y="-2.8194"/>
<vertex x="2.8194" y="-2.667"/>
<vertex x="2.9718" y="-2.4892"/>
<vertex x="3.1242" y="-2.3114"/>
<vertex x="2.5146" y="-1.397"/>
<vertex x="2.6416" y="-1.1938"/>
<vertex x="2.7178" y="-0.9652"/>
<vertex x="2.794" y="-0.7366"/>
<vertex x="2.8448" y="-0.5588"/>
<vertex x="3.8608" y="-0.381"/>
<vertex x="3.8862" y="-0.1016"/>
<vertex x="3.8862" y="0.127"/>
<vertex x="3.8608" y="0.3302"/>
<vertex x="3.8354" y="0.5842"/>
<vertex x="2.794" y="0.762"/>
<vertex x="2.6924" y="1.0414"/>
<vertex x="2.6416" y="1.1938"/>
<vertex x="2.54" y="1.3716"/>
<vertex x="2.413" y="1.5748"/>
<vertex x="3.048" y="2.3876"/>
<vertex x="2.8956" y="2.5654"/>
<vertex x="2.7178" y="2.7432"/>
<vertex x="2.5146" y="2.921"/>
<vertex x="2.3114" y="3.0988"/>
<vertex x="1.4478" y="2.4892"/>
<vertex x="1.2192" y="2.5908"/>
<vertex x="1.016" y="2.6924"/>
<vertex x="0.8382" y="2.7432"/>
<vertex x="0.6604" y="2.794"/>
<vertex x="0.508" y="3.8354"/>
<vertex x="0.2286" y="3.8354"/>
<vertex x="0" y="3.8354"/>
<vertex x="-0.2794" y="3.8354"/>
<vertex x="-0.4572" y="3.8354"/>
<vertex x="-0.6604" y="2.794"/>
<vertex x="-0.9398" y="2.7178"/>
<vertex x="-1.143" y="2.6162"/>
<vertex x="-1.4478" y="2.4638"/>
<vertex x="-2.3114" y="3.0734"/>
<vertex x="-2.4892" y="2.921"/>
<vertex x="-2.667" y="2.7432"/>
<vertex x="-2.8448" y="2.5654"/>
<vertex x="-2.9972" y="2.3876"/>
<vertex x="-2.413" y="1.524"/>
<vertex x="-2.54" y="1.2954"/>
<vertex x="-2.6416" y="1.0668"/>
<vertex x="-2.7178" y="0.8636"/>
<vertex x="-2.794" y="0.6604"/>
<vertex x="-3.81" y="0.508"/>
<vertex x="-3.8354" y="0.2032"/>
<vertex x="-3.8354" y="0"/>
<vertex x="-3.8354" y="-0.2286"/>
<vertex x="-3.8354" y="-0.4572"/>
<vertex x="-2.7686" y="-0.635"/>
<vertex x="-2.7178" y="-0.889"/>
<vertex x="-2.6162" y="-1.143"/>
<vertex x="-2.5146" y="-1.3716"/>
<vertex x="-2.413" y="-1.5748"/>
<vertex x="-3.0226" y="-2.3876"/>
<vertex x="-2.8702" y="-2.5654"/>
<vertex x="-2.6924" y="-2.7432"/>
<vertex x="-2.5146" y="-2.921"/>
<vertex x="-2.3622" y="-3.048"/>
<vertex x="-1.4986" y="-2.4638"/>
<vertex x="-1.3208" y="-2.5654"/>
<vertex x="-1.143" y="-2.6416"/>
<vertex x="-0.9906" y="-2.7178"/>
</polygon>
<text x="4.8006" y="-3.6576" size="1.27" layer="25" rot="R180">open source
  hardware</text>
</package>
<package name="OSHW_5MM">
<polygon width="0.0508" layer="25">
<vertex x="-0.2413" y="-0.6096" curve="-160.717601"/>
<vertex x="0" y="0.6604" curve="-152.950596"/>
<vertex x="0.2794" y="-0.6096"/>
<vertex x="0.6731" y="-1.6002"/>
<vertex x="0.8255" y="-1.5367"/>
<vertex x="0.9779" y="-1.4351"/>
<vertex x="1.4859" y="-1.7907"/>
<vertex x="1.5875" y="-1.7018"/>
<vertex x="1.6764" y="-1.6129"/>
<vertex x="1.778" y="-1.4986"/>
<vertex x="1.8669" y="-1.3843"/>
<vertex x="1.4986" y="-0.8509"/>
<vertex x="1.5875" y="-0.6985"/>
<vertex x="1.6383" y="-0.5461"/>
<vertex x="1.6891" y="-0.3429"/>
<vertex x="2.3114" y="-0.2286"/>
<vertex x="2.3114" y="-0.0508"/>
<vertex x="2.3114" y="0.0635"/>
<vertex x="2.2987" y="0.2032"/>
<vertex x="2.286" y="0.3302"/>
<vertex x="1.6637" y="0.4445"/>
<vertex x="1.6129" y="0.6096"/>
<vertex x="1.5494" y="0.7493"/>
<vertex x="1.4605" y="0.9017"/>
<vertex x="1.4478" y="0.9271"/>
<vertex x="1.8034" y="1.4224"/>
<vertex x="1.7018" y="1.5494"/>
<vertex x="1.5748" y="1.6764"/>
<vertex x="1.4732" y="1.7653"/>
<vertex x="1.3843" y="1.8288"/>
<vertex x="0.8636" y="1.4605"/>
<vertex x="0.7493" y="1.524"/>
<vertex x="0.635" y="1.5748"/>
<vertex x="0.5207" y="1.6256"/>
<vertex x="0.4064" y="1.651"/>
<vertex x="0.2921" y="2.2733"/>
<vertex x="0.1397" y="2.286"/>
<vertex x="-0.0381" y="2.286"/>
<vertex x="-0.1524" y="2.286"/>
<vertex x="-0.2794" y="2.2733"/>
<vertex x="-0.381" y="1.6637"/>
<vertex x="-0.5715" y="1.6002"/>
<vertex x="-0.6985" y="1.5494"/>
<vertex x="-0.8763" y="1.4478"/>
<vertex x="-1.3843" y="1.8161"/>
<vertex x="-1.4986" y="1.7272"/>
<vertex x="-1.6256" y="1.6002"/>
<vertex x="-1.7272" y="1.4859"/>
<vertex x="-1.7907" y="1.4224"/>
<vertex x="-1.4351" y="0.9017"/>
<vertex x="-1.5113" y="0.7747"/>
<vertex x="-1.5875" y="0.6096"/>
<vertex x="-1.6256" y="0.4699"/>
<vertex x="-1.6637" y="0.381"/>
<vertex x="-2.286" y="0.2794"/>
<vertex x="-2.286" y="-0.2794"/>
<vertex x="-1.6637" y="-0.381"/>
<vertex x="-1.6129" y="-0.5588"/>
<vertex x="-1.5748" y="-0.6858"/>
<vertex x="-1.524" y="-0.7874"/>
<vertex x="-1.4351" y="-0.9398"/>
<vertex x="-1.8034" y="-1.4478"/>
<vertex x="-1.7018" y="-1.5621"/>
<vertex x="-1.6129" y="-1.651"/>
<vertex x="-1.4986" y="-1.7526"/>
<vertex x="-1.4097" y="-1.8288"/>
<vertex x="-0.889" y="-1.4732"/>
<vertex x="-0.5969" y="-1.6256"/>
</polygon>
<text x="2.921" y="-2.032" size="0.762" layer="25" rot="R180">open source
  hardware</text>
</package>
</packages>
<symbols>
<symbol name="OSHWLOGO">
<wire x1="0" y1="0" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="17.78" y2="7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="7.62" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="94">OSHW Logo</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="OSHWLOGO" prefix="GOLD_ORB_SM">
<description>&lt;p&gt;The OSHW logo for PCB layout</description>
<gates>
<gate name="G$1" symbol="OSHWLOGO" x="0" y="0"/>
</gates>
<devices>
<device name="LOGO16MM" package="OSHW_16MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOGO8MM" package="OSHW_8MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOGO5MM" package="OSHW_5MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="S11" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="1"/>
<part name="S12" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="2"/>
<part name="S13" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="3"/>
<part name="S14" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="4"/>
<part name="S15" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="5"/>
<part name="S16" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="6"/>
<part name="S17" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="7"/>
<part name="S18" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="8"/>
<part name="S19" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="9"/>
<part name="S110" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="0"/>
<part name="S21" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="ESC"/>
<part name="S22" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="q"/>
<part name="S23" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="w"/>
<part name="S24" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="e"/>
<part name="S25" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="r"/>
<part name="S26" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="t"/>
<part name="S27" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="y"/>
<part name="S28" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="u"/>
<part name="S29" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="i"/>
<part name="S210" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="o"/>
<part name="S31" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="CTRL"/>
<part name="S32" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="a"/>
<part name="S33" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="s"/>
<part name="S34" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="d"/>
<part name="S35" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="f"/>
<part name="S36" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="g"/>
<part name="S37" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="h"/>
<part name="S38" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="j"/>
<part name="S39" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="k"/>
<part name="S310" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="l"/>
<part name="S41" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="CTRL"/>
<part name="S42" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="a"/>
<part name="S43" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="s"/>
<part name="S44" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="d"/>
<part name="S45" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="f"/>
<part name="S46" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="g"/>
<part name="S47" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="h"/>
<part name="S48" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="j"/>
<part name="S49" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="k"/>
<part name="S410" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="l"/>
<part name="S111" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="0"/>
<part name="S211" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="o"/>
<part name="S311" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="l"/>
<part name="B1" library="SparkFun-Boards" library_urn="urn:adsk.eagle:library:12590577" deviceset="SPARKFUN_PRO_MICRO" device="" package3d_urn="urn:adsk.eagle:package:12590700/1"/>
<part name="RESET" library="SparkFun-Switches" library_urn="urn:adsk.eagle:library:12047409" deviceset="MOMENTARY-SWITCH-SPST" device="-PTH-6.0MM" package3d_urn="urn:adsk.eagle:package:12047493/6" value="l"/>
<part name="JP2" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-2X20" device="" package3d_urn="urn:adsk.eagle:package:22443/2" value="Pi Header"/>
<part name="DISPLAY" library="con-hirose" library_urn="urn:adsk.eagle:library:152" deviceset="FH12-16S-1S" device="H" package3d_urn="urn:adsk.eagle:package:7325/1"/>
<part name="JP1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-2X12" device="" package3d_urn="urn:adsk.eagle:package:22420/2" value="Expansion"/>
<part name="JP3" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-2X4" device="" package3d_urn="urn:adsk.eagle:package:22461/2" value="pi extra"/>
<part name="GOLD_ORB_SM1" library="oshw" deviceset="OSHWLOGO" device="LOGO5MM"/>
<part name="JP4" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X6" device="/90" package3d_urn="urn:adsk.eagle:package:22475/2" value="ardinuo extra"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="S11" gate="G$1" x="68.58" y="-33.02" smashed="yes">
<attribute name="NAME" x="68.58" y="-31.496" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-33.528" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S12" gate="G$1" x="68.58" y="-40.64" smashed="yes">
<attribute name="NAME" x="68.58" y="-39.116" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-41.148" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S13" gate="G$1" x="68.58" y="-48.26" smashed="yes">
<attribute name="NAME" x="68.58" y="-46.736" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-48.768" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S14" gate="G$1" x="68.58" y="-55.88" smashed="yes">
<attribute name="NAME" x="68.58" y="-54.356" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-56.388" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S15" gate="G$1" x="68.58" y="-63.5" smashed="yes">
<attribute name="NAME" x="68.58" y="-61.976" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-64.008" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S16" gate="G$1" x="68.58" y="-71.12" smashed="yes">
<attribute name="NAME" x="68.58" y="-69.596" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-71.628" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S17" gate="G$1" x="68.58" y="-78.74" smashed="yes">
<attribute name="NAME" x="68.58" y="-77.216" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-79.248" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S18" gate="G$1" x="68.58" y="-86.36" smashed="yes">
<attribute name="NAME" x="68.58" y="-84.836" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-86.868" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S19" gate="G$1" x="68.58" y="-93.98" smashed="yes">
<attribute name="NAME" x="68.58" y="-92.456" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-94.488" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S110" gate="G$1" x="68.58" y="-101.6" smashed="yes">
<attribute name="NAME" x="68.58" y="-100.076" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-102.108" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S21" gate="G$1" x="83.82" y="-33.02" smashed="yes">
<attribute name="NAME" x="83.82" y="-31.496" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-33.528" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S22" gate="G$1" x="83.82" y="-40.64" smashed="yes">
<attribute name="NAME" x="83.82" y="-39.116" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-41.148" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S23" gate="G$1" x="83.82" y="-48.26" smashed="yes">
<attribute name="NAME" x="83.82" y="-46.736" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-48.768" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S24" gate="G$1" x="83.82" y="-55.88" smashed="yes">
<attribute name="NAME" x="83.82" y="-54.356" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-56.388" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S25" gate="G$1" x="83.82" y="-63.5" smashed="yes">
<attribute name="NAME" x="83.82" y="-61.976" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-64.008" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S26" gate="G$1" x="83.82" y="-71.12" smashed="yes">
<attribute name="NAME" x="83.82" y="-69.596" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-71.628" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S27" gate="G$1" x="83.82" y="-78.74" smashed="yes">
<attribute name="NAME" x="83.82" y="-77.216" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-79.248" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S28" gate="G$1" x="83.82" y="-86.36" smashed="yes">
<attribute name="NAME" x="83.82" y="-84.836" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-86.868" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S29" gate="G$1" x="83.82" y="-93.98" smashed="yes">
<attribute name="NAME" x="83.82" y="-92.456" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-94.488" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S210" gate="G$1" x="83.82" y="-101.6" smashed="yes">
<attribute name="NAME" x="83.82" y="-100.076" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-102.108" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S31" gate="G$1" x="99.06" y="-33.02" smashed="yes">
<attribute name="NAME" x="99.06" y="-31.496" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-33.528" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S32" gate="G$1" x="99.06" y="-40.64" smashed="yes">
<attribute name="NAME" x="99.06" y="-39.116" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-41.148" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S33" gate="G$1" x="99.06" y="-48.26" smashed="yes">
<attribute name="NAME" x="99.06" y="-46.736" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-48.768" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S34" gate="G$1" x="99.06" y="-55.88" smashed="yes">
<attribute name="NAME" x="99.06" y="-54.356" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-56.388" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S35" gate="G$1" x="99.06" y="-63.5" smashed="yes">
<attribute name="NAME" x="99.06" y="-61.976" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-64.008" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S36" gate="G$1" x="99.06" y="-71.12" smashed="yes">
<attribute name="NAME" x="99.06" y="-69.596" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-71.628" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S37" gate="G$1" x="99.06" y="-78.74" smashed="yes">
<attribute name="NAME" x="99.06" y="-77.216" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-79.248" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S38" gate="G$1" x="99.06" y="-86.36" smashed="yes">
<attribute name="NAME" x="99.06" y="-84.836" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-86.868" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S39" gate="G$1" x="99.06" y="-93.98" smashed="yes">
<attribute name="NAME" x="99.06" y="-92.456" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-94.488" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S310" gate="G$1" x="99.06" y="-101.6" smashed="yes">
<attribute name="NAME" x="99.06" y="-100.076" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-102.108" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S41" gate="G$1" x="114.3" y="-33.02" smashed="yes">
<attribute name="NAME" x="114.3" y="-31.496" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-33.528" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S42" gate="G$1" x="114.3" y="-40.64" smashed="yes">
<attribute name="NAME" x="114.3" y="-39.116" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-41.148" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S43" gate="G$1" x="114.3" y="-48.26" smashed="yes">
<attribute name="NAME" x="114.3" y="-46.736" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-48.768" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S44" gate="G$1" x="114.3" y="-55.88" smashed="yes">
<attribute name="NAME" x="114.3" y="-54.356" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-56.388" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S45" gate="G$1" x="114.3" y="-63.5" smashed="yes">
<attribute name="NAME" x="114.3" y="-61.976" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-64.008" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S46" gate="G$1" x="114.3" y="-71.12" smashed="yes">
<attribute name="NAME" x="114.3" y="-69.596" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-71.628" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S47" gate="G$1" x="114.3" y="-78.74" smashed="yes">
<attribute name="NAME" x="114.3" y="-77.216" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-79.248" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S48" gate="G$1" x="114.3" y="-86.36" smashed="yes">
<attribute name="NAME" x="114.3" y="-84.836" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-86.868" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S49" gate="G$1" x="114.3" y="-93.98" smashed="yes">
<attribute name="NAME" x="114.3" y="-92.456" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-94.488" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S410" gate="G$1" x="114.3" y="-101.6" smashed="yes">
<attribute name="NAME" x="114.3" y="-100.076" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="114.3" y="-102.108" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S111" gate="G$1" x="68.58" y="-109.22" smashed="yes">
<attribute name="NAME" x="68.58" y="-107.696" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="-109.728" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S211" gate="G$1" x="83.82" y="-109.22" smashed="yes">
<attribute name="NAME" x="83.82" y="-107.696" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="83.82" y="-109.728" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="S311" gate="G$1" x="99.06" y="-109.22" smashed="yes">
<attribute name="NAME" x="99.06" y="-107.696" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="-109.728" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="B1" gate="G$1" x="17.78" y="-7.62" smashed="yes">
<attribute name="NAME" x="10.16" y="10.922" size="1.778" layer="95"/>
<attribute name="VALUE" x="10.16" y="-25.4" size="1.778" layer="96"/>
</instance>
<instance part="RESET" gate="G$1" x="43.18" y="17.78" smashed="yes">
<attribute name="NAME" x="43.18" y="19.304" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="43.18" y="17.272" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="JP2" gate="A" x="200.66" y="-63.5" smashed="yes">
<attribute name="VALUE" x="194.31" y="-93.98" size="1.778" layer="96"/>
</instance>
<instance part="DISPLAY" gate="-1" x="309.88" y="-40.64" smashed="yes">
<attribute name="NAME" x="311.912" y="-41.402" size="1.778" layer="95"/>
<attribute name="VALUE" x="307.34" y="-38.1" size="1.778" layer="96"/>
</instance>
<instance part="DISPLAY" gate="-2" x="309.88" y="-43.18" smashed="yes">
<attribute name="NAME" x="311.912" y="-43.942" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-3" x="309.88" y="-45.72" smashed="yes">
<attribute name="NAME" x="311.912" y="-46.482" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-4" x="309.88" y="-48.26" smashed="yes">
<attribute name="NAME" x="311.912" y="-49.022" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-5" x="309.88" y="-50.8" smashed="yes">
<attribute name="NAME" x="311.912" y="-51.562" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-6" x="309.88" y="-53.34" smashed="yes">
<attribute name="NAME" x="311.912" y="-54.102" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-7" x="309.88" y="-55.88" smashed="yes">
<attribute name="NAME" x="311.912" y="-56.642" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-8" x="309.88" y="-58.42" smashed="yes">
<attribute name="NAME" x="311.912" y="-59.182" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-9" x="309.88" y="-60.96" smashed="yes">
<attribute name="NAME" x="311.912" y="-61.722" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-10" x="309.88" y="-63.5" smashed="yes">
<attribute name="NAME" x="311.912" y="-64.262" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-11" x="309.88" y="-66.04" smashed="yes">
<attribute name="NAME" x="311.912" y="-66.802" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-12" x="309.88" y="-68.58" smashed="yes">
<attribute name="NAME" x="311.912" y="-69.342" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-13" x="309.88" y="-71.12" smashed="yes">
<attribute name="NAME" x="311.912" y="-71.882" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-14" x="309.88" y="-73.66" smashed="yes">
<attribute name="NAME" x="311.912" y="-74.422" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-15" x="309.88" y="-76.2" smashed="yes">
<attribute name="NAME" x="311.912" y="-76.962" size="1.778" layer="95"/>
</instance>
<instance part="DISPLAY" gate="-16" x="309.88" y="-78.74" smashed="yes">
<attribute name="NAME" x="311.912" y="-79.502" size="1.778" layer="95"/>
</instance>
<instance part="JP1" gate="A" x="200.66" y="7.62" smashed="yes">
<attribute name="NAME" x="194.31" y="23.495" size="1.778" layer="95"/>
<attribute name="VALUE" x="194.31" y="-12.7" size="1.778" layer="96"/>
</instance>
<instance part="JP3" gate="A" x="124.46" y="17.78" smashed="yes">
<attribute name="NAME" x="118.11" y="26.035" size="1.778" layer="95"/>
<attribute name="VALUE" x="118.11" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="GOLD_ORB_SM1" gate="G$1" x="271.78" y="0" smashed="yes"/>
<instance part="JP4" gate="A" x="83.82" y="-132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="73.025" y="-138.43" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="93.98" y="-138.43" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<pinref part="S110" gate="G$1" pin="2"/>
<pinref part="S19" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-101.6" x2="73.66" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="S18" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-93.98" x2="73.66" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="S17" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-86.36" x2="73.66" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="S16" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-78.74" x2="73.66" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="S15" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-71.12" x2="73.66" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="S14" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-63.5" x2="73.66" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="S13" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-55.88" x2="73.66" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="S12" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-48.26" x2="73.66" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="S11" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-40.64" x2="73.66" y2="-33.02" width="0.1524" layer="91"/>
<junction x="73.66" y="-40.64"/>
<junction x="73.66" y="-48.26"/>
<junction x="73.66" y="-55.88"/>
<junction x="73.66" y="-63.5"/>
<junction x="73.66" y="-71.12"/>
<junction x="73.66" y="-78.74"/>
<junction x="73.66" y="-86.36"/>
<junction x="73.66" y="-93.98"/>
<wire x1="73.66" y1="-33.02" x2="73.66" y2="-5.08" width="0.1524" layer="91"/>
<junction x="73.66" y="-33.02"/>
<pinref part="S111" gate="G$1" pin="2"/>
<wire x1="73.66" y1="-109.22" x2="73.66" y2="-101.6" width="0.1524" layer="91"/>
<junction x="73.66" y="-101.6"/>
<pinref part="B1" gate="G$1" pin="A2"/>
<wire x1="73.66" y1="-5.08" x2="30.48" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-109.22" x2="73.66" y2="-116.84" width="0.1524" layer="91"/>
<junction x="73.66" y="-109.22"/>
<pinref part="JP4" gate="A" pin="2"/>
<wire x1="78.74" y1="-134.62" x2="78.74" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-116.84" x2="73.66" y2="-116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="S210" gate="G$1" pin="2"/>
<pinref part="S29" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-93.98" x2="88.9" y2="-101.6" width="0.1524" layer="91"/>
<junction x="88.9" y="-93.98"/>
<pinref part="S28" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-86.36" x2="88.9" y2="-93.98" width="0.1524" layer="91"/>
<junction x="88.9" y="-86.36"/>
<pinref part="S27" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-78.74" x2="88.9" y2="-86.36" width="0.1524" layer="91"/>
<junction x="88.9" y="-78.74"/>
<pinref part="S26" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-71.12" x2="88.9" y2="-78.74" width="0.1524" layer="91"/>
<junction x="88.9" y="-71.12"/>
<pinref part="S25" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-63.5" x2="88.9" y2="-71.12" width="0.1524" layer="91"/>
<junction x="88.9" y="-63.5"/>
<pinref part="S24" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-55.88" x2="88.9" y2="-63.5" width="0.1524" layer="91"/>
<junction x="88.9" y="-55.88"/>
<pinref part="S23" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-48.26" x2="88.9" y2="-55.88" width="0.1524" layer="91"/>
<junction x="88.9" y="-48.26"/>
<pinref part="S22" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-40.64" x2="88.9" y2="-48.26" width="0.1524" layer="91"/>
<junction x="88.9" y="-40.64"/>
<pinref part="S21" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-33.02" x2="88.9" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-33.02" x2="88.9" y2="-2.54" width="0.1524" layer="91"/>
<junction x="88.9" y="-33.02"/>
<pinref part="S211" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-101.6" x2="88.9" y2="-109.22" width="0.1524" layer="91"/>
<junction x="88.9" y="-101.6"/>
<pinref part="B1" gate="G$1" pin="A3"/>
<wire x1="30.48" y1="-2.54" x2="88.9" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-121.92" x2="88.9" y2="-109.22" width="0.1524" layer="91"/>
<junction x="88.9" y="-109.22"/>
<wire x1="88.9" y1="-121.92" x2="76.2" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="JP4" gate="A" pin="1"/>
<wire x1="76.2" y1="-121.92" x2="76.2" y2="-134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="S41" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-27.94" x2="109.22" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-27.94" x2="93.98" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="S31" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-27.94" x2="93.98" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="S21" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-27.94" x2="78.74" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-27.94" x2="78.74" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="S11" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-27.94" x2="63.5" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-27.94" x2="63.5" y2="-33.02" width="0.1524" layer="91"/>
<junction x="63.5" y="-27.94"/>
<junction x="78.74" y="-27.94"/>
<junction x="93.98" y="-27.94"/>
<wire x1="5.08" y1="-27.94" x2="63.5" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="*9"/>
<wire x1="5.08" y1="-27.94" x2="5.08" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="5.08" y1="-20.32" x2="7.62" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="S44" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-50.8" x2="109.22" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-50.8" x2="93.98" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="S34" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-50.8" x2="93.98" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="S24" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-50.8" x2="78.74" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-50.8" x2="78.74" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="S14" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-50.8" x2="63.5" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-50.8" x2="63.5" y2="-55.88" width="0.1524" layer="91"/>
<junction x="78.74" y="-50.8"/>
<junction x="63.5" y="-50.8"/>
<junction x="93.98" y="-50.8"/>
<wire x1="43.18" y1="-50.8" x2="63.5" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-12.7" x2="43.18" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="13(SCK)"/>
<wire x1="30.48" y1="-12.7" x2="43.18" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="S45" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-58.42" x2="109.22" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-58.42" x2="93.98" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="S35" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-58.42" x2="93.98" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="S25" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-58.42" x2="78.74" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-58.42" x2="78.74" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="S15" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-58.42" x2="63.5" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-58.42" x2="63.5" y2="-63.5" width="0.1524" layer="91"/>
<junction x="63.5" y="-58.42"/>
<junction x="78.74" y="-58.42"/>
<junction x="93.98" y="-58.42"/>
<pinref part="B1" gate="G$1" pin="8"/>
<wire x1="-10.16" y1="-58.42" x2="63.5" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-17.78" x2="-10.16" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="S46" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-66.04" x2="109.22" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-66.04" x2="93.98" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="S36" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-66.04" x2="93.98" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="S26" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-66.04" x2="78.74" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-66.04" x2="78.74" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="S16" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-66.04" x2="63.5" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-66.04" x2="63.5" y2="-71.12" width="0.1524" layer="91"/>
<junction x="78.74" y="-66.04"/>
<junction x="63.5" y="-66.04"/>
<wire x1="-12.7" y1="-66.04" x2="63.5" y2="-66.04" width="0.1524" layer="91"/>
<junction x="93.98" y="-66.04"/>
<wire x1="-12.7" y1="-66.04" x2="-12.7" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="7"/>
<wire x1="-12.7" y1="-15.24" x2="7.62" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="S47" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-73.66" x2="109.22" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-73.66" x2="93.98" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="S37" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-73.66" x2="93.98" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="S27" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-73.66" x2="78.74" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-73.66" x2="78.74" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="S17" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-73.66" x2="63.5" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-73.66" x2="63.5" y2="-78.74" width="0.1524" layer="91"/>
<junction x="63.5" y="-73.66"/>
<junction x="78.74" y="-73.66"/>
<junction x="93.98" y="-73.66"/>
<pinref part="B1" gate="G$1" pin="*6"/>
<wire x1="-15.24" y1="-73.66" x2="63.5" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-12.7" x2="-15.24" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-12.7" x2="-15.24" y2="-73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="S48" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-81.28" x2="109.22" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-81.28" x2="93.98" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="S38" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-81.28" x2="93.98" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="S28" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-81.28" x2="78.74" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-81.28" x2="78.74" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="S18" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-81.28" x2="63.5" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-81.28" x2="63.5" y2="-86.36" width="0.1524" layer="91"/>
<junction x="78.74" y="-81.28"/>
<junction x="63.5" y="-81.28"/>
<wire x1="-17.78" y1="-81.28" x2="63.5" y2="-81.28" width="0.1524" layer="91"/>
<junction x="93.98" y="-81.28"/>
<pinref part="B1" gate="G$1" pin="*5"/>
<wire x1="7.62" y1="-10.16" x2="-17.78" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-10.16" x2="-17.78" y2="-81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="S49" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-88.9" x2="109.22" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-88.9" x2="93.98" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="S39" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-88.9" x2="93.98" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="S29" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-88.9" x2="78.74" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-88.9" x2="78.74" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="S19" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-88.9" x2="63.5" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-88.9" x2="63.5" y2="-93.98" width="0.1524" layer="91"/>
<junction x="63.5" y="-88.9"/>
<junction x="78.74" y="-88.9"/>
<wire x1="-20.32" y1="-88.9" x2="63.5" y2="-88.9" width="0.1524" layer="91"/>
<junction x="93.98" y="-88.9"/>
<wire x1="-20.32" y1="-88.9" x2="-20.32" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="4"/>
<wire x1="-20.32" y1="-7.62" x2="7.62" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="S410" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-96.52" x2="109.22" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-96.52" x2="93.98" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="S310" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-96.52" x2="93.98" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="S210" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-96.52" x2="78.74" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-96.52" x2="78.74" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="S110" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-96.52" x2="63.5" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-96.52" x2="63.5" y2="-101.6" width="0.1524" layer="91"/>
<junction x="78.74" y="-96.52"/>
<junction x="63.5" y="-96.52"/>
<junction x="93.98" y="-96.52"/>
<pinref part="B1" gate="G$1" pin="*3"/>
<wire x1="-22.86" y1="-96.52" x2="63.5" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-5.08" x2="-22.86" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-5.08" x2="-22.86" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TP_IRQ" class="0">
<segment>
<pinref part="JP2" gate="A" pin="11"/>
<wire x1="198.12" y1="-53.34" x2="180.34" y2="-53.34" width="0.1524" layer="91"/>
<label x="180.34" y="-53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-5" pin="1"/>
<wire x1="307.34" y1="-50.8" x2="302.26" y2="-50.8" width="0.1524" layer="91"/>
<label x="302.26" y="-50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LCD_RS" class="0">
<segment>
<pinref part="JP2" gate="A" pin="18"/>
<wire x1="205.74" y1="-60.96" x2="213.36" y2="-60.96" width="0.1524" layer="91"/>
<label x="213.36" y="-60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-7" pin="1"/>
<wire x1="307.34" y1="-55.88" x2="302.26" y2="-55.88" width="0.1524" layer="91"/>
<label x="302.26" y="-55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LCD_SI" class="0">
<segment>
<pinref part="JP2" gate="A" pin="19"/>
<wire x1="198.12" y1="-63.5" x2="180.34" y2="-63.5" width="0.1524" layer="91"/>
<label x="180.34" y="-63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-8" pin="1"/>
<wire x1="307.34" y1="-58.42" x2="287.02" y2="-58.42" width="0.1524" layer="91"/>
<label x="287.02" y="-58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TP_SO" class="0">
<segment>
<pinref part="JP2" gate="A" pin="21"/>
<wire x1="198.12" y1="-66.04" x2="190.5" y2="-66.04" width="0.1524" layer="91"/>
<label x="190.5" y="-66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-10" pin="1"/>
<wire x1="307.34" y1="-63.5" x2="287.02" y2="-63.5" width="0.1524" layer="91"/>
<label x="287.02" y="-63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="JP2" gate="A" pin="22"/>
<wire x1="205.74" y1="-66.04" x2="213.36" y2="-66.04" width="0.1524" layer="91"/>
<label x="213.36" y="-66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-11" pin="1"/>
<wire x1="307.34" y1="-66.04" x2="302.26" y2="-66.04" width="0.1524" layer="91"/>
<label x="302.26" y="-66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LCD_SCK" class="0">
<segment>
<pinref part="JP2" gate="A" pin="23"/>
<wire x1="198.12" y1="-68.58" x2="180.34" y2="-68.58" width="0.1524" layer="91"/>
<label x="180.34" y="-68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-12" pin="1"/>
<wire x1="307.34" y1="-68.58" x2="287.02" y2="-68.58" width="0.1524" layer="91"/>
<label x="287.02" y="-68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LCD_CS" class="0">
<segment>
<pinref part="JP2" gate="A" pin="24"/>
<wire x1="205.74" y1="-68.58" x2="226.06" y2="-68.58" width="0.1524" layer="91"/>
<label x="226.06" y="-68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-13" pin="1"/>
<wire x1="307.34" y1="-71.12" x2="302.26" y2="-71.12" width="0.1524" layer="91"/>
<label x="302.26" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TP_CS" class="0">
<segment>
<pinref part="JP2" gate="A" pin="26"/>
<wire x1="205.74" y1="-71.12" x2="213.36" y2="-71.12" width="0.1524" layer="91"/>
<label x="213.36" y="-71.12" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-14" pin="1"/>
<wire x1="307.34" y1="-73.66" x2="287.02" y2="-73.66" width="0.1524" layer="91"/>
<label x="287.02" y="-73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="3V" class="0">
<segment>
<pinref part="JP2" gate="A" pin="17"/>
<wire x1="198.12" y1="-60.96" x2="190.5" y2="-60.96" width="0.1524" layer="91"/>
<label x="190.5" y="-60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="1"/>
<wire x1="198.12" y1="-40.64" x2="190.5" y2="-40.64" width="0.1524" layer="91"/>
<label x="190.5" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-3" pin="1"/>
<wire x1="307.34" y1="-45.72" x2="302.26" y2="-45.72" width="0.1524" layer="91"/>
<label x="302.26" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-6" pin="1"/>
<wire x1="307.34" y1="-53.34" x2="287.02" y2="-53.34" width="0.1524" layer="91"/>
<label x="287.02" y="-53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="205.74" y1="2.54" x2="215.9" y2="2.54" width="0.1524" layer="91"/>
<label x="215.9" y="2.54" size="1.778" layer="95" xref="yes"/>
<pinref part="JP1" gate="A" pin="16"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<wire x1="198.12" y1="5.08" x2="190.5" y2="5.08" width="0.1524" layer="91"/>
<label x="190.5" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="13"/>
</segment>
<segment>
<wire x1="129.54" y1="22.86" x2="137.16" y2="22.86" width="0.1524" layer="91"/>
<label x="137.16" y="22.86" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="2"/>
<wire x1="205.74" y1="-40.64" x2="213.36" y2="-40.64" width="0.1524" layer="91"/>
<label x="213.36" y="-40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="4"/>
<wire x1="205.74" y1="-43.18" x2="226.06" y2="-43.18" width="0.1524" layer="91"/>
<label x="226.06" y="-43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-1" pin="1"/>
<wire x1="307.34" y1="-40.64" x2="302.26" y2="-40.64" width="0.1524" layer="91"/>
<label x="302.26" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-2" pin="1"/>
<wire x1="307.34" y1="-43.18" x2="287.02" y2="-43.18" width="0.1524" layer="91"/>
<label x="287.02" y="-43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="215.9" y1="17.78" x2="205.74" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="4"/>
<label x="215.9" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<label x="190.5" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="198.12" y1="20.32" x2="190.5" y2="20.32" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="1"/>
</segment>
<segment>
<pinref part="B1" gate="G$1" pin="GND@2"/>
<wire x1="30.48" y1="5.08" x2="43.18" y2="5.08" width="0.1524" layer="91"/>
<label x="43.18" y="5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="B1" gate="G$1" pin="GND"/>
<wire x1="7.62" y1="0" x2="2.54" y2="0" width="0.1524" layer="91"/>
<label x="2.54" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="B1" gate="G$1" pin="GND@1"/>
<wire x1="7.62" y1="2.54" x2="-17.78" y2="2.54" width="0.1524" layer="91"/>
<label x="-17.78" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RESET" gate="G$1" pin="1"/>
<wire x1="38.1" y1="22.86" x2="38.1" y2="17.78" width="0.1524" layer="91"/>
<wire x1="38.1" y1="22.86" x2="35.56" y2="22.86" width="0.1524" layer="91"/>
<label x="35.56" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="14"/>
<wire x1="205.74" y1="-55.88" x2="213.36" y2="-55.88" width="0.1524" layer="91"/>
<label x="213.36" y="-55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="20"/>
<label x="226.06" y="-63.5" size="1.778" layer="95" xref="yes"/>
<wire x1="205.74" y1="-63.5" x2="226.06" y2="-63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="30"/>
<wire x1="205.74" y1="-76.2" x2="213.36" y2="-76.2" width="0.1524" layer="91"/>
<label x="213.36" y="-76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="25"/>
<wire x1="198.12" y1="-71.12" x2="190.5" y2="-71.12" width="0.1524" layer="91"/>
<label x="190.5" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="39"/>
<wire x1="198.12" y1="-88.9" x2="180.34" y2="-88.9" width="0.1524" layer="91"/>
<label x="180.34" y="-88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="34"/>
<wire x1="205.74" y1="-81.28" x2="213.36" y2="-81.28" width="0.1524" layer="91"/>
<label x="213.36" y="-81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="6"/>
<wire x1="205.74" y1="-45.72" x2="213.36" y2="-45.72" width="0.1524" layer="91"/>
<label x="213.36" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="9"/>
<wire x1="198.12" y1="-50.8" x2="190.5" y2="-50.8" width="0.1524" layer="91"/>
<label x="190.5" y="-50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-4" pin="1"/>
<wire x1="307.34" y1="-48.26" x2="287.02" y2="-48.26" width="0.1524" layer="91"/>
<label x="287.02" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-9" pin="1"/>
<wire x1="307.34" y1="-60.96" x2="302.26" y2="-60.96" width="0.1524" layer="91"/>
<label x="302.26" y="-60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="5"/>
<wire x1="121.92" y1="17.78" x2="104.14" y2="17.78" width="0.1524" layer="91"/>
<label x="104.14" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="198.12" y1="10.16" x2="187.96" y2="10.16" width="0.1524" layer="91"/>
<label x="190.5" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP1" gate="A" pin="9"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="S42" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-35.56" x2="109.22" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-35.56" x2="93.98" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="S32" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-35.56" x2="93.98" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="S22" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-35.56" x2="78.74" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-35.56" x2="78.74" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="S12" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-35.56" x2="63.5" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-35.56" x2="63.5" y2="-40.64" width="0.1524" layer="91"/>
<junction x="63.5" y="-35.56"/>
<junction x="78.74" y="-35.56"/>
<junction x="93.98" y="-35.56"/>
<wire x1="33.02" y1="-35.56" x2="63.5" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-35.56" x2="33.02" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="11(MOSI)"/>
<wire x1="30.48" y1="-17.78" x2="33.02" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="S43" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-43.18" x2="109.22" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-43.18" x2="93.98" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="S33" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-43.18" x2="93.98" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="S23" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-43.18" x2="78.74" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-43.18" x2="78.74" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="S13" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-43.18" x2="63.5" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-43.18" x2="63.5" y2="-48.26" width="0.1524" layer="91"/>
<junction x="63.5" y="-43.18"/>
<junction x="78.74" y="-43.18"/>
<junction x="93.98" y="-43.18"/>
<wire x1="38.1" y1="-43.18" x2="63.5" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-43.18" x2="38.1" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="12(MISO)"/>
<wire x1="38.1" y1="-15.24" x2="30.48" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ARX" class="0">
<segment>
<pinref part="B1" gate="G$1" pin="RXI"/>
<wire x1="7.62" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="91"/>
<label x="2.54" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="228.6" y1="20.32" x2="205.74" y2="20.32" width="0.1524" layer="91"/>
<label x="228.6" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="ATX" class="0">
<segment>
<wire x1="180.34" y1="17.78" x2="198.12" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="3"/>
<label x="180.34" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="B1" gate="G$1" pin="TXO"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.1524" layer="91"/>
<label x="-7.62" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="B1" gate="G$1" pin="RESET"/>
<wire x1="30.48" y1="2.54" x2="58.42" y2="2.54" width="0.1524" layer="91"/>
<pinref part="RESET" gate="G$1" pin="2"/>
<wire x1="48.26" y1="25.4" x2="48.26" y2="17.78" width="0.1524" layer="91"/>
<wire x1="58.42" y1="2.54" x2="58.42" y2="25.4" width="0.1524" layer="91"/>
<wire x1="58.42" y1="25.4" x2="48.26" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="S311" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-104.14" x2="93.98" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="S211" gate="G$1" pin="1"/>
<wire x1="93.98" y1="-104.14" x2="78.74" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-104.14" x2="78.74" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="S111" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-104.14" x2="63.5" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-104.14" x2="63.5" y2="-109.22" width="0.1524" layer="91"/>
<junction x="78.74" y="-104.14"/>
<junction x="63.5" y="-104.14"/>
<wire x1="-25.4" y1="-104.14" x2="63.5" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="-104.14" x2="-25.4" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-2.54" x2="7.62" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="S410" gate="G$1" pin="2"/>
<pinref part="S49" gate="G$1" pin="2"/>
<junction x="119.38" y="-93.98"/>
<pinref part="S48" gate="G$1" pin="2"/>
<junction x="119.38" y="-86.36"/>
<pinref part="S47" gate="G$1" pin="2"/>
<junction x="119.38" y="-78.74"/>
<pinref part="S46" gate="G$1" pin="2"/>
<junction x="119.38" y="-71.12"/>
<pinref part="S45" gate="G$1" pin="2"/>
<junction x="119.38" y="-63.5"/>
<pinref part="S44" gate="G$1" pin="2"/>
<junction x="119.38" y="-55.88"/>
<pinref part="S43" gate="G$1" pin="2"/>
<pinref part="S42" gate="G$1" pin="2"/>
<pinref part="B1" gate="G$1" pin="A0"/>
<wire x1="30.48" y1="-10.16" x2="119.38" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="S41" gate="G$1" pin="2"/>
<wire x1="119.38" y1="-33.02" x2="119.38" y2="-10.16" width="0.1524" layer="91"/>
<junction x="119.38" y="-33.02"/>
<wire x1="119.38" y1="-33.02" x2="119.38" y2="-40.64" width="0.1524" layer="91"/>
<junction x="119.38" y="-40.64"/>
<wire x1="119.38" y1="-40.64" x2="119.38" y2="-48.26" width="0.1524" layer="91"/>
<junction x="119.38" y="-48.26"/>
<wire x1="119.38" y1="-48.26" x2="119.38" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-55.88" x2="119.38" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-63.5" x2="119.38" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-71.12" x2="119.38" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-78.74" x2="119.38" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-86.36" x2="119.38" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-93.98" x2="119.38" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-134.62" x2="119.38" y2="-101.6" width="0.1524" layer="91"/>
<junction x="119.38" y="-101.6"/>
<pinref part="JP4" gate="A" pin="6"/>
<wire x1="88.9" y1="-134.62" x2="119.38" y2="-134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="S311" gate="G$1" pin="2"/>
<pinref part="S310" gate="G$1" pin="2"/>
<pinref part="S39" gate="G$1" pin="2"/>
<junction x="104.14" y="-93.98"/>
<pinref part="S38" gate="G$1" pin="2"/>
<junction x="104.14" y="-86.36"/>
<pinref part="S37" gate="G$1" pin="2"/>
<junction x="104.14" y="-78.74"/>
<pinref part="S36" gate="G$1" pin="2"/>
<junction x="104.14" y="-71.12"/>
<pinref part="S35" gate="G$1" pin="2"/>
<junction x="104.14" y="-63.5"/>
<pinref part="S34" gate="G$1" pin="2"/>
<junction x="104.14" y="-55.88"/>
<pinref part="S33" gate="G$1" pin="2"/>
<pinref part="S32" gate="G$1" pin="2"/>
<pinref part="B1" gate="G$1" pin="A1"/>
<wire x1="30.48" y1="-7.62" x2="104.14" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="S31" gate="G$1" pin="2"/>
<wire x1="104.14" y1="-33.02" x2="104.14" y2="-7.62" width="0.1524" layer="91"/>
<junction x="104.14" y="-33.02"/>
<wire x1="104.14" y1="-33.02" x2="104.14" y2="-40.64" width="0.1524" layer="91"/>
<junction x="104.14" y="-40.64"/>
<wire x1="104.14" y1="-40.64" x2="104.14" y2="-48.26" width="0.1524" layer="91"/>
<junction x="104.14" y="-48.26"/>
<wire x1="104.14" y1="-48.26" x2="104.14" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-55.88" x2="104.14" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-63.5" x2="104.14" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-71.12" x2="104.14" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-78.74" x2="104.14" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-86.36" x2="104.14" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-93.98" x2="104.14" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-101.6" x2="104.14" y2="-109.22" width="0.1524" layer="91"/>
<junction x="104.14" y="-101.6"/>
<wire x1="86.36" y1="-127" x2="109.22" y2="-127" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-127" x2="109.22" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-109.22" x2="104.14" y2="-109.22" width="0.1524" layer="91"/>
<junction x="104.14" y="-109.22"/>
<pinref part="JP4" gate="A" pin="5"/>
<wire x1="86.36" y1="-127" x2="86.36" y2="-134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5VA" class="0">
<segment>
<pinref part="B1" gate="G$1" pin="VCC"/>
<wire x1="30.48" y1="0" x2="43.18" y2="0" width="0.1524" layer="91"/>
<label x="43.18" y="0" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="121.92" y1="22.86" x2="104.14" y2="22.86" width="0.1524" layer="91"/>
<label x="104.14" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="A" pin="1"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="B1" gate="G$1" pin="*10"/>
<wire x1="30.48" y1="-20.32" x2="53.34" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-20.32" x2="53.34" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="JP4" gate="A" pin="3"/>
<pinref part="JP4" gate="A" pin="4"/>
<wire x1="83.82" y1="-134.62" x2="81.28" y2="-134.62" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-124.46" x2="81.28" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-124.46" x2="81.28" y2="-134.62" width="0.1524" layer="91"/>
<junction x="81.28" y="-134.62"/>
</segment>
</net>
<net name="GP3" class="0">
<segment>
<pinref part="JP2" gate="A" pin="5"/>
<wire x1="198.12" y1="-45.72" x2="182.88" y2="-45.72" width="0.1524" layer="91"/>
<label x="182.88" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="226.06" y1="5.08" x2="205.74" y2="5.08" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="14"/>
<label x="226.06" y="5.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PRX" class="0">
<segment>
<wire x1="215.9" y1="7.62" x2="205.74" y2="7.62" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="12"/>
<label x="215.9" y="7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="A" pin="10"/>
<wire x1="205.74" y1="-50.8" x2="213.36" y2="-50.8" width="0.1524" layer="91"/>
<label x="213.36" y="-50.8" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GP27" class="0">
<segment>
<pinref part="JP2" gate="A" pin="13"/>
<wire x1="198.12" y1="-55.88" x2="190.5" y2="-55.88" width="0.1524" layer="91"/>
<label x="190.5" y="-55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="10"/>
<wire x1="233.68" y1="10.16" x2="205.74" y2="10.16" width="0.1524" layer="91"/>
<label x="228.6" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GP22" class="0">
<segment>
<pinref part="JP2" gate="A" pin="15"/>
<wire x1="198.12" y1="-58.42" x2="180.34" y2="-58.42" width="0.1524" layer="91"/>
<label x="180.34" y="-58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="215.9" y1="12.7" x2="205.74" y2="12.7" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="8"/>
<label x="215.9" y="12.7" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="ID_SD" class="0">
<segment>
<pinref part="JP2" gate="A" pin="27"/>
<wire x1="198.12" y1="-73.66" x2="180.34" y2="-73.66" width="0.1524" layer="91"/>
<label x="180.34" y="-73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="5"/>
<wire x1="190.5" y1="15.24" x2="198.12" y2="15.24" width="0.1524" layer="91"/>
<label x="190.5" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PTX" class="0">
<segment>
<wire x1="226.06" y1="-48.26" x2="205.74" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="8"/>
<label x="226.06" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="7"/>
<wire x1="198.12" y1="12.7" x2="180.34" y2="12.7" width="0.1524" layer="91"/>
<label x="180.34" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GP23" class="0">
<segment>
<pinref part="JP2" gate="A" pin="16"/>
<wire x1="205.74" y1="-58.42" x2="226.06" y2="-58.42" width="0.1524" layer="91"/>
<label x="226.06" y="-58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="17"/>
<wire x1="190.5" y1="0" x2="198.12" y2="0" width="0.1524" layer="91"/>
<label x="190.5" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ID_SC" class="0">
<segment>
<pinref part="JP2" gate="A" pin="28"/>
<wire x1="205.74" y1="-73.66" x2="226.06" y2="-73.66" width="0.1524" layer="91"/>
<label x="226.06" y="-73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="20"/>
<wire x1="215.9" y1="-2.54" x2="205.74" y2="-2.54" width="0.1524" layer="91"/>
<label x="215.9" y="-2.54" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GP19" class="0">
<segment>
<pinref part="JP2" gate="A" pin="35"/>
<wire x1="198.12" y1="-83.82" x2="180.34" y2="-83.82" width="0.1524" layer="91"/>
<label x="180.34" y="-83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="22"/>
<wire x1="226.06" y1="-5.08" x2="205.74" y2="-5.08" width="0.1524" layer="91"/>
<label x="226.06" y="-5.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="L2" class="0">
<segment>
<pinref part="JP1" gate="A" pin="23"/>
<wire x1="180.34" y1="-7.62" x2="198.12" y2="-7.62" width="0.1524" layer="91"/>
<label x="180.34" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="8"/>
<wire x1="129.54" y1="15.24" x2="147.32" y2="15.24" width="0.1524" layer="91"/>
<label x="147.32" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GP26" class="0">
<segment>
<pinref part="JP2" gate="A" pin="37"/>
<wire x1="190.5" y1="-86.36" x2="198.12" y2="-86.36" width="0.1524" layer="91"/>
<label x="190.5" y="-86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="137.16" y1="17.78" x2="129.54" y2="17.78" width="0.1524" layer="91"/>
<pinref part="JP3" gate="A" pin="6"/>
<label x="137.16" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="L1" class="0">
<segment>
<pinref part="JP3" gate="A" pin="7"/>
<wire x1="114.3" y1="15.24" x2="121.92" y2="15.24" width="0.1524" layer="91"/>
<label x="114.3" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="21"/>
<wire x1="198.12" y1="-5.08" x2="187.96" y2="-5.08" width="0.1524" layer="91"/>
<label x="187.96" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GP6" class="0">
<segment>
<pinref part="JP2" gate="A" pin="31"/>
<wire x1="198.12" y1="-78.74" x2="180.34" y2="-78.74" width="0.1524" layer="91"/>
<label x="180.34" y="-78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="228.6" y1="15.24" x2="205.74" y2="15.24" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="6"/>
<label x="228.6" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GP18" class="0">
<segment>
<pinref part="JP2" gate="A" pin="12"/>
<wire x1="205.74" y1="-53.34" x2="226.06" y2="-53.34" width="0.1524" layer="91"/>
<label x="226.06" y="-53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="226.06" y1="0" x2="205.74" y2="0" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="18"/>
<label x="226.06" y="0" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GP16" class="0">
<segment>
<pinref part="JP2" gate="A" pin="36"/>
<wire x1="226.06" y1="-83.82" x2="205.74" y2="-83.82" width="0.1524" layer="91"/>
<label x="226.06" y="-83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="205.74" y1="-7.62" x2="215.9" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="24"/>
<label x="215.9" y="-7.62" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GP13" class="0">
<segment>
<pinref part="JP2" gate="A" pin="33"/>
<wire x1="198.12" y1="-81.28" x2="190.5" y2="-81.28" width="0.1524" layer="91"/>
<label x="190.5" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="180.34" y1="-2.54" x2="198.12" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="19"/>
<label x="180.34" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GP4" class="0">
<segment>
<pinref part="JP2" gate="A" pin="7"/>
<wire x1="198.12" y1="-48.26" x2="165.1" y2="-48.26" width="0.1524" layer="91"/>
<label x="165.1" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="180.34" y1="7.62" x2="198.12" y2="7.62" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="11"/>
<label x="180.34" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GP2" class="0">
<segment>
<pinref part="JP2" gate="A" pin="3"/>
<wire x1="198.12" y1="-43.18" x2="167.64" y2="-43.18" width="0.1524" layer="91"/>
<label x="167.64" y="-43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="180.34" y1="2.54" x2="198.12" y2="2.54" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="15"/>
<label x="180.34" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GP12" class="0">
<segment>
<pinref part="JP2" gate="A" pin="32"/>
<wire x1="205.74" y1="-78.74" x2="226.06" y2="-78.74" width="0.1524" layer="91"/>
<label x="226.06" y="-78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-16" pin="1"/>
<wire x1="307.34" y1="-78.74" x2="287.02" y2="-78.74" width="0.1524" layer="91"/>
<label x="287.02" y="-78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GP5" class="0">
<segment>
<pinref part="JP2" gate="A" pin="29"/>
<wire x1="198.12" y1="-76.2" x2="190.5" y2="-76.2" width="0.1524" layer="91"/>
<label x="190.5" y="-76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DISPLAY" gate="-15" pin="1"/>
<wire x1="307.34" y1="-76.2" x2="302.26" y2="-76.2" width="0.1524" layer="91"/>
<label x="302.26" y="-76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GP21" class="0">
<segment>
<pinref part="JP2" gate="A" pin="40"/>
<wire x1="205.74" y1="-88.9" x2="226.06" y2="-88.9" width="0.1524" layer="91"/>
<label x="226.06" y="-88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="147.32" y1="20.32" x2="129.54" y2="20.32" width="0.1524" layer="91"/>
<pinref part="JP3" gate="A" pin="4"/>
<label x="147.32" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="GP20" class="0">
<segment>
<pinref part="JP2" gate="A" pin="38"/>
<wire x1="205.74" y1="-86.36" x2="213.36" y2="-86.36" width="0.1524" layer="91"/>
<label x="213.36" y="-86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="3"/>
<wire x1="121.92" y1="20.32" x2="114.3" y2="20.32" width="0.1524" layer="91"/>
<label x="114.3" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
