# mutantC
# Made by rahmanshaber( @rahmanshaber:matrix.org )
# Checkout the poject website from [here](https://mutantc.gitlab.io).
### A Raspberry-pi handheld platform with a physical keyboard, Display and Expansion header for custom baords (Like Ardinuo Shield).
### It is fully open-source hardware with GPLv3 or later license. So you can modify or hack it as you wish.
### It is platform like Arduino. You can make your expansion-card like gps, Radio etc.
### So help us to make a Community around it.
### [Parts list](https://gitlab.com/mutantC/mutantc-v1/blob/master/parts_list).

# Download
### Download the project from [here](https://a360.co/2o37ZCc).


# Software used
### Autodesk fusion 360 - 3d parts
### Eagle - PCB
### Ardinuo IDE - keyboard code

<img src="p1.JPG" width="500"> 
<img src="p2.JPG" width="500"> 
<img src="p6.JPG" width="500"> 
<img src="p3.png" width="500">
<img src="p4.png" width="500"> 
<img src="p5.png" width="500">
<img src="p7.png" width="500">
<img src="p8.png" width="500">
<img src="p9.png" width="500">
<img src="p10.png" width="500">
<img src="p11.png" width="500">
<img src="p12.png" width="500">

# Feedback
We need your feedback to improve the mutantC.
Send us your feedback through GitLab [issues](https://gitlab.com/groups/mutantC/-/issues).
